package com.credix.cm.promptpay.commonsjpa.model.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@Data
@EqualsAndHashCode(of = "id")
@Where(clause = "BOO_DELETED=false")
public abstract class GenericEntity implements Serializable {
    @Id
    @GeneratedValue
    @Column(name = "ID")
    protected UUID id;
    @Column(name = "CREATED_ON")
    protected LocalDateTime createdOn;
    @Column(name = "UPDATED_ON")
    protected LocalDateTime updatedOn;
    @Column(name = "BOO_DELETED")
    protected Boolean deleted=false;

    @PrePersist
    protected void onCreate() {
        createdOn = LocalDateTime.now();
    }

    @PreUpdate
    protected void onUpdate() {
        updatedOn = LocalDateTime.now();
    }
}

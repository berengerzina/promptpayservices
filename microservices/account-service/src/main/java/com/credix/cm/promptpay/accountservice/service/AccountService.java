package com.credix.cm.promptpay.accountservice.service;

import com.credix.cm.promptpay.accountservice.model.Account;
import com.credix.cm.promptpay.accountservice.repository.IAccountRepository;
import com.credix.cm.promptpay.accountservice.utils.AccountUtils;
import com.credix.cm.promptpay.commonsmodel.models.transaction.AccountTransfer;
import com.credix.cm.promptpay.commonsmodel.models.transaction.EPaymentSens;
import com.credix.cm.promptpay.commonsmodel.utils.GenericEntityUtils;
import com.credix.cm.promptpay.commonsstream.event.DataEvent;
import com.credix.cm.promptpay.commonsstream.event.EventType;
import com.credix.cm.promptpay.commonsstream.event.data.*;
import com.credix.cm.promptpay.commonsstream.response.AccountStatusResponse;
import com.credix.cm.promptpay.commonsstream.stream.StreamNamesConstant;
import com.credix.cm.promptpay.commonsstream.utils.AbstractStreamBridgeAwareBean;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.credix.cm.promptpay.commonsstream.stream.StreamNamesConstant.PAYMENT_STREAM_NAME;

/**
 * @author Berenger Zina
 * created on 22/07/2021
 * Project promptpay-services
 **/

@Slf4j
@Service
public class AccountService extends AbstractStreamBridgeAwareBean {
    private final AccountUtils accountUtils;
    private final IAccountRepository accountRepository;
    private final GenericEntityUtils genericEntityUtils;

    public AccountService(AccountUtils accountUtils, IAccountRepository accountRepository, GenericEntityUtils genericEntityUtils) {
        this.accountUtils = accountUtils;
        this.accountRepository = accountRepository;
        this.genericEntityUtils = genericEntityUtils;
    }

    public Mono<Void> createAccount(DataEvent<UUID, AccountData> dataEvent) {
        return accountUtils.fromAccountDataToAccountMono(dataEvent.getData())
                .map(account -> genericEntityUtils.fillEntityGenericInfos(account, dataEvent.getKey(), dataEvent.getEventDate()))
                .flatMap(accountRepository::save)
                .doOnSuccess(account -> doOnAccountCreationSuccess(dataEvent, account))
                .then();
    }

    @SneakyThrows
    private void doOnAccountCreationSuccess(DataEvent<UUID, AccountData> dataEvent, Account account) {
        var accountStatusResponse = AccountStatusResponse.builder().accountHolderName(account.getSubscriptionLite().getClientName())
                .accountNumber(account.getAccountNumber())
                .balance(account.getAccountBalance())
                .build();
        this.sendNotificationEvent(dataEvent, new ObjectMapper().writeValueAsString(accountStatusResponse));
    }

    public Mono<Void> doCashIn(DataEvent<UUID, CashInData> event) {
        log.info("Initiating cash in event {}", event);
        return accountRepository.findAccountBySubscriptionLitePhoneNumberAndActive(event.getData().getPhoneNumber(), true)
                .doOnNext(account -> {
                    var paymentData = PaymentData.builder()
                            .phoneNumber(account.getSubscriptionLite().getPhoneNumber())
                            .transactionAmount(event.getData().getCashInAmount())
                            .originalEventId(event.getKey())
                            .destinationAccountId(account.getId())
                            .originalEventType(EventType.CASH_IN)
                            .paymentMethod(account.getSubscriptionLite().getPaymentMethod())
                            .deviseISO(account.getSubscriptionLite().getDeviseISO())
                            .note("PromptPay")
                            .message("PromptPay-CashIn")
                            .build();
                    log.info("payment data for cash in {}", paymentData);
                    var dataEvent = new DataEvent<>(UUID.randomUUID(), paymentData, LocalDateTime.now(), EventType.CASH_IN_PAYMENT);
                    log.info("Sending payment event for cash in {}", dataEvent);
                    this.getStreamBridge().send(PAYMENT_STREAM_NAME, dataEvent);
                })
                .then();
    }

    public Mono<Void> validateCashIn(DataEvent<UUID, CashInData> event) {
        log.info("Doing validation post processing for cash in event {}", event);
        return accountRepository.findAccountBySubscriptionLitePhoneNumberAndActive(event.getData().getPhoneNumber(), true)
                .doOnNext(account -> {
                    account.setAccountBalance(account.getAccountBalance().add(event.getData().getPayment().getAmount()));
                    this.addTransferToAccount(account, event.getData().getPayment());
                    accountRepository.save(account).subscribe();
                })
                .doOnError(throwable -> this.doOnCashInValidationError(throwable, event))
                .then();
    }

    @SneakyThrows
    private void doOnCashInValidationError(Throwable throwable, DataEvent<UUID, CashInData> event) {
        log.error("An error occurred during payment validation", throwable);
        event.getData().setNumberOfRetry(event.getData().getNumberOfRetry() + 1);
        this.getStreamBridge().send(StreamNamesConstant.CASH_IN_ERROR_STREAM_NAME, event);
    }

    public Mono<Void> doTransfer(DataEvent<UUID, TransferData> event) {
        log.info("Initiating transfer between source {} and dest {} with amount {}", event.getData().getSourceAccount(),
                event.getData().getDestinationAccount(), event.getData().getTransferAmount());
        return accountRepository.findAccountByAccountNumber(event.getData().getSourceAccount())
                .filter(account -> {
                    if (account.getAccountBalance().compareTo(event.getData().getTransferAmount()) < 0) {
                        this.sendInsufficientFundNotification(account, event, event.getData().getTransferAmount());
                    }
                    return account.getAccountBalance().compareTo(event.getData().getTransferAmount()) >= 0;
                })
                .zipWith(accountRepository.findAccountByAccountNumber(event.getData().getDestinationAccount()))
                .map(tuple -> PaymentData.builder()
                        .transactionAmount(event.getData().getTransferAmount())
                        .originalEventId(event.getKey())
                        .sourceAccountId(tuple.getT1().getId())
                        .sourceAccountNumber(tuple.getT1().getAccountNumber())
                        .destinationAccountId(tuple.getT2().getId())
                        .destinationAccountNumber(tuple.getT2().getAccountNumber())
                        .originalEventType(EventType.TRANSFER)
                        .build())
                .doOnNext(paymentData -> {
                    log.info("payment data for transfer in {}", paymentData);
                    var dataEvent = new DataEvent<>(UUID.randomUUID(), paymentData, LocalDateTime.now(), EventType.TRANSFER);
                    log.info("Sending payment event for transfer {}", dataEvent);
                    this.getStreamBridge().send(PAYMENT_STREAM_NAME, dataEvent);
                })
                .then();
    }

    private void sendInsufficientFundNotification(Account account, DataEvent<UUID, ? extends AbstractData> event, BigDecimal amountRequested) {
        log.info("account {} has insufficient funds to request {}", account, amountRequested);
        //ToDO use message bundles here
        this.sendNotificationEvent(event, "account " + account + " has insufficient funds to request " + amountRequested);
    }

    public Mono<Void> validateTransfer(DataEvent<UUID, TransferData> event) {
        log.info("Doing validation post processing for transfer event {}", event);
        return accountRepository.findAccountByAccountNumber(event.getData().getSourceAccount())
                .zipWith(accountRepository.findAccountByAccountNumber(event.getData().getDestinationAccount()))
                .doOnNext(tuple -> {
                    tuple.getT1().setAccountBalance(tuple.getT1().getAccountBalance().subtract(event.getData().getTransferAmount()));
                    this.addTransferToAccount(tuple.getT1(), event.getData().getAccountTransfer());
                    tuple.getT2().setAccountBalance(tuple.getT2().getAccountBalance().add(event.getData().getTransferAmount()));
                    this.addTransferToAccount(tuple.getT2(), event.getData().getAccountTransfer());
                    accountRepository.save(tuple.getT1()).then(accountRepository.save(tuple.getT2())).subscribe();
                })
                .then();
    }

    private void addTransferToAccount(Account account, AccountTransfer accountTransfer) {
        log.info("Adding transfer {} to account {}", accountTransfer, account);
        var transfers = account.getAccountTransfers();
        transfers.add(accountTransfer.cloneAccountTransfer());
        transfers = transfers.stream()
                .sorted(Comparator.comparing(AccountTransfer::getPaymentDate))
                .limit(10)
                .map(transfer -> {
                    transfer.setPaymentSens(this.computePaymentSens(account, transfer));
                    return transfer;
                })
                .collect(Collectors.toSet());
        account.getAccountTransfers().clear();
        account.getAccountTransfers().addAll(transfers);
    }

    private EPaymentSens computePaymentSens(Account account, AccountTransfer accountTransfer) {
        if (account.getId().equals(accountTransfer.getSourceAccountId())) {
            return EPaymentSens.DEBIT;
        } else if (account.getId().equals(accountTransfer.getDestinationAccountId())) {
            return EPaymentSens.CREDIT;
        } else {
            throw new IllegalStateException("Payment sens can't be determined");
        }
    }
}

package com.credix.cm.promptpay.accountservice.model;

/**
 * @author Berenger Zina
 * created on 22/07/2021
 * Project promptpay-services
 **/

public enum EAccountType {
    BASIC("01", "9011008");
    private String code;
    private String prefix;

    EAccountType(String code, String prefix) {
        this.code = code;
        this.prefix = prefix;
    }

    public String getCode() {
        return code;
    }

    public String getPrefix() {
        return prefix;
    }
}

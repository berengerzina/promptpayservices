package com.credix.cm.promptpay.accountservice.utils;

import com.credix.cm.promptpay.accountservice.model.Account;
import com.credix.cm.promptpay.accountservice.model.EAccountType;
import com.credix.cm.promptpay.commonsstream.event.data.AccountData;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.math.BigDecimal;
import java.util.HashSet;

/**
 * @author Berenger Zina
 * created on 22/07/2021
 * Project promptpay-services
 **/

@Component
public class AccountUtils {
    private final AccountNumberHelper accountNumberHelper;

    public AccountUtils(AccountNumberHelper accountNumberHelper) {
        this.accountNumberHelper = accountNumberHelper;
    }

    public Mono<Account> fromAccountDataToAccountMono(AccountData data) {
        var account = Account.builder().subscriptionLite(data.getSubscription())
                .accountBalance(BigDecimal.ZERO)
                .accountInitialBalance(BigDecimal.ZERO)
                .active(true)
                .accountType(EAccountType.BASIC)
                .accountTransfers(new HashSet<>())
                .build();
        return Mono.just(account).zipWith(accountNumberHelper.generateAccountNumber(account.getAccountType()))
                .map(tuple -> {
                    var acc = tuple.getT1();
                    acc.setAccountNumber(tuple.getT2());
                    return acc;
                });
    }
}

package com.credix.cm.promptpay.accountservice.message;

import com.credix.cm.promptpay.accountservice.service.AccountService;
import com.credix.cm.promptpay.commonsstream.event.DataEvent;
import com.credix.cm.promptpay.commonsstream.event.EventType;
import com.credix.cm.promptpay.commonsstream.event.data.AbstractData;
import com.credix.cm.promptpay.commonsstream.event.data.AccountData;
import com.credix.cm.promptpay.commonsstream.event.data.CashInData;
import com.credix.cm.promptpay.commonsstream.event.data.TransferData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.UUID;
import java.util.function.Consumer;

/**
 * @author Berenger Zina
 * created on 22/07/2021
 * Project promptpay-services
 **/

@Slf4j
@Component
public class MessageConsumer {
    private final AccountService accountService;

    public MessageConsumer(AccountService accountService) {
        this.accountService = accountService;
    }

    @Bean
    public Consumer<DataEvent<UUID, AccountData>> accountConsumer() {
        return event -> {
            this.logEventInfos(event);
            if (event.getEventType() == EventType.ACCOUNT_CREATION) {
                accountService.createAccount(event).subscribe();
            } else {
                doOnOperationNotImplemented();
            }
        };
    }

    private void doOnOperationNotImplemented() {
        throw new UnsupportedOperationException("event handler not present for this consumer");
    }

    @Bean
    public Consumer<DataEvent<UUID, CashInData>> cashInConsumer() {
        return event -> {
            this.logEventInfos(event);
            switch (event.getEventType()) {
                case CASH_IN:
                    accountService.doCashIn(event).subscribe();
                    break;
                case CASH_IN_VALIDATION:
                    accountService.validateCashIn(event).subscribe();
                    break;
                default:
                    doOnOperationNotImplemented();
            }
        };
    }

    @Bean
    public Consumer<DataEvent<UUID, TransferData>> transferConsumer() {
        return event -> {
            this.logEventInfos(event);
            switch (event.getEventType()) {
                case TRANSFER:
                    accountService.doTransfer(event).subscribe();
                    break;
                case TRANSFER_VALIDATION:
                    accountService.validateTransfer(event).subscribe();
                    break;
                default:
                    doOnOperationNotImplemented();
            }
        };
    }

    private <T extends AbstractData> void logEventInfos(DataEvent<UUID, T> event) {
        log.info("Inbound message type {}, date {}, data {}", event.getEventType(), event.getEventDate(), event.getData());
    }
}

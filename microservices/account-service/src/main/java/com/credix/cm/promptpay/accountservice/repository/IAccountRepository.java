package com.credix.cm.promptpay.accountservice.repository;

import com.credix.cm.promptpay.accountservice.model.Account;
import org.springframework.data.elasticsearch.repository.ReactiveElasticsearchRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

import java.util.UUID;

/**
 * @author Berenger Zina
 * created on 22/07/2021
 * Project promptpay-services
 **/

@Repository
public interface IAccountRepository extends ReactiveElasticsearchRepository<Account, UUID> {
    Mono<Account> findAccountBySubscriptionLitePhoneNumberAndActive(String phoneNumber, boolean active);

    Mono<Account> findAccountByAccountNumber(String accountNumber);
}

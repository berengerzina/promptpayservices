package com.credix.cm.promptpay.accountservice.model;

import com.credix.cm.promptpay.commonsmodel.models.generic.GenericEntity;
import com.credix.cm.promptpay.commonsmodel.models.subscription.SubscriptionLite;
import com.credix.cm.promptpay.commonsmodel.models.transaction.AccountTransfer;
import lombok.*;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.data.elasticsearch.core.query.SeqNoPrimaryTerm;

import java.math.BigDecimal;
import java.util.Set;

/**
 * @author Berenger Zina
 * created on 22/07/2021
 * Project promptpay-services
 **/

@Builder
@Data
@EqualsAndHashCode(of = "accountNumber", callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@ToString(of = {"accountNumber", "accountBalance", "accountType", "active"})
@Document(indexName = "account")
public class Account extends GenericEntity {
    private String accountNumber;
    private BigDecimal accountBalance;
    private BigDecimal accountInitialBalance;
    private boolean active;
    private EAccountType accountType;
    @Field(type = FieldType.Nested, includeInParent = true)
    private SubscriptionLite subscriptionLite;
    private SeqNoPrimaryTerm seqNoPrimaryTerm;
    private Set<AccountTransfer> accountTransfers;
}

package com.credix.cm.promptpay.accountservice.utils;

import com.credix.cm.promptpay.accountservice.model.EAccountType;
import com.credix.cm.promptpay.accountservice.repository.IAccountRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 * @author Berenger Zina
 * created on 22/07/2021
 * Project promptpay-services
 **/

@Slf4j
@Component
public class AccountNumberHelper {
    public final IAccountRepository accountRepository;

    public AccountNumberHelper(IAccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public Mono<String> generateAccountNumber(EAccountType accountType) {
        log.info("Generating account number for {}", accountType);
        Mono<Long> nextVal;
        switch (accountType) {
            case BASIC:
                nextVal = accountRepository.count();
                break;
            default:
                log.warn("Account type {} not supported", accountType);
                throw new UnsupportedOperationException("Account type not supported");
        }
        NumberFormat numberFormat = new DecimalFormat("00000000");
        return nextVal.zipWith(Mono.just(accountType))
                .map(tuple -> tuple.getT2().getPrefix().concat(numberFormat.format(tuple.getT1()).concat(tuple.getT2().getCode())));
    }
}

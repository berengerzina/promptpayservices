package com.credix.cm.promptpay.paymentservice.mobilemoney.momoapi.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

/**
 * @author Berenger Zina
 * created on 22/07/2021
 * Project promptpay-services
 **/

@Getter
@Setter
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MoMoRequestToPayStatus extends MoMoRequestToPayBody {
    private String financialTransactionId;
    private ERequestTOPayStatus status;
    private String reason;

    @Builder(builderMethodName = "moMoRequestToPayStatus")
    public MoMoRequestToPayStatus(Integer amount, String currency, UUID externalId, MoMoPayee payee, String payerMessage, String payeeNote,
                                  String financialTransactionId, ERequestTOPayStatus status, String reason) {
        super(amount, currency, externalId, payee, payerMessage, payeeNote);
        this.financialTransactionId = financialTransactionId;
        this.status = status;
        this.reason = reason;
    }

    public enum ERequestTOPayStatus {
        SUCCESSFUL, FAILED, PENDING;
    }
}

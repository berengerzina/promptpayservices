package com.credix.cm.promptpay.paymentservice.repository;

import com.credix.cm.promptpay.paymentservice.model.Transaction;
import org.springframework.data.elasticsearch.repository.ReactiveElasticsearchRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

/**
 * @author Berenger Zina
 * created on 22/07/2021
 * Project promptpay-services
 **/

@Repository
public interface ITransactionRepository extends ReactiveElasticsearchRepository<Transaction, UUID> {
}

package com.credix.cm.promptpay.paymentservice.message;

import com.credix.cm.promptpay.commonsstream.event.DataEvent;
import com.credix.cm.promptpay.commonsstream.event.EventType;
import com.credix.cm.promptpay.commonsstream.event.data.PaymentData;
import com.credix.cm.promptpay.commonsstream.event.data.PaymentValidationData;
import com.credix.cm.promptpay.paymentservice.mobilemoney.PaymentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.UUID;
import java.util.function.Consumer;

/**
 * @author Berenger Zina
 * created on 22/07/2021
 * Project promptpay-services
 **/

@Slf4j
@Component
public class MessageConsumer {
    private final PaymentService paymentService;

    public MessageConsumer(PaymentService paymentService) {
        this.paymentService = paymentService;
    }

    @Bean
    public Consumer<DataEvent<UUID, PaymentData>> paymentConsumer() {
        return event -> {
            log.info("Inbound message type {}, date {}, data {}", event.getEventType(), event.getEventDate(), event.getData());
            switch (event.getEventType()){
                case SUBSCRIPTION_FEE_PAYMENT:
                case CASH_IN_PAYMENT:
                case TRANSFER:
                    paymentService.processToPayment(event).subscribe();
                    break;
                default:
                    throw new UnsupportedOperationException(event.getEventType().name().concat(" is not handled in this stream"));
            }
        };
    }

    @Bean
    public Consumer<DataEvent<UUID, PaymentValidationData>> paymentValidation() {
        return event -> {
            log.info("Inbound message type {}, date {}, data {}", event.getEventType(), event.getEventDate(), event.getData());
            if (event.getEventType() == EventType.PAYMENT_VALIDATION) {
                paymentService.validatePayment(event).subscribe();
            } else {
                throw new UnsupportedOperationException(event.getEventType().name().concat(" is not handled in this stream"));
            }
        };
    }
}

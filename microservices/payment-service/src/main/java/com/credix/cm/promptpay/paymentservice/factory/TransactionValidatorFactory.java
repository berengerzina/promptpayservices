package com.credix.cm.promptpay.paymentservice.factory;

import com.credix.cm.promptpay.commonsmodel.models.transaction.EPaymentState;
import com.credix.cm.promptpay.commonsstream.event.EventType;
import com.credix.cm.promptpay.commonsstream.event.data.PaymentValidationData;
import com.credix.cm.promptpay.paymentservice.mobilemoney.momoapi.MoMoApiHelper;
import com.credix.cm.promptpay.paymentservice.mobilemoney.momoapi.model.MoMoRequestToPayStatus;
import com.credix.cm.promptpay.paymentservice.model.Transaction;
import com.credix.cm.promptpay.paymentservice.repository.ITransactionRepository;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;

import java.util.function.Function;

/**
 * @author beren
 * created on 27/07/2021
 * Project promptpay-services
 **/

@Slf4j
@Component
public class TransactionValidatorFactory {
    private final MoMoApiHelper moMoApiHelper;
    private final ITransactionRepository transactionRepository;

    public TransactionValidatorFactory(MoMoApiHelper moMoApiHelper, ITransactionRepository transactionRepository) {
        this.moMoApiHelper = moMoApiHelper;
        this.transactionRepository = transactionRepository;
    }

    public Function<PaymentValidationData, Mono<Transaction>> getOperationFunction(EventType eventType) {
        switch (eventType) {
            case SUBSCRIPTION_FEE_PAYMENT:
            case CASH_IN_PAYMENT:
                return this.momoCashInValidator();
            case TRANSFER:
                return this.transferValidator();
            default:
                log.warn("No validator defined for type {}", eventType);
                throw new UnsupportedOperationException("Operation not handled yet");
        }
    }

    private Function<PaymentValidationData, Mono<Transaction>> momoCashInValidator() {
        return paymentValidationData -> moMoApiHelper.validatePayment(paymentValidationData).zipWith(transactionRepository.findById(paymentValidationData.getTransactionId()))
                .map(momoPaymentValidationFunction(paymentValidationData))
                .flatMap(transactionRepository::save);
    }

    @NotNull
    private Function<Tuple2<MoMoRequestToPayStatus, Transaction>, Transaction> momoPaymentValidationFunction(PaymentValidationData paymentValidationData) {
        return tuple -> {
            if (MoMoRequestToPayStatus.ERequestTOPayStatus.SUCCESSFUL == tuple.getT1().getStatus()) {
                log.info("Transaction {} was successful", tuple.getT2().getId());
                tuple.getT2().setPaymentState(EPaymentState.VALIDATED);
                tuple.getT2().getExternalTransactionDetails().setFinancialTransactionId(tuple.getT1().getFinancialTransactionId());
            } else if (MoMoRequestToPayStatus.ERequestTOPayStatus.FAILED == tuple.getT1().getStatus()) {
                log.info("Transaction {} was unsuccessful reason {}", tuple.getT2().getId(), tuple.getT1().getReason());
                tuple.getT2().setPaymentState(EPaymentState.FAILED);
                tuple.getT2().getExternalTransactionDetails().setErrorCode(tuple.getT1().getReason());
            } else {
                log.warn("Transaction {} is in unexpected MOMO status {}", paymentValidationData.getTransactionId(), tuple.getT1().getStatus());
                throw new IllegalStateException("Transaction is in unexpected MOMO status");
            }
            return tuple.getT2();
        };
    }

    private Function<PaymentValidationData, Mono<Transaction>> transferValidator() {
        return paymentValidationData -> transactionRepository.findById(paymentValidationData.getTransactionId())
                .map(transaction -> {
                    transaction.setPaymentState(EPaymentState.VALIDATED);
                    return transaction;
                })
                .flatMap(transactionRepository::save);
    }
}

package com.credix.cm.promptpay.paymentservice.mobilemoney.momoapi.model;

import com.fasterxml.jackson.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.annotation.Generated;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Berenger Zina
 * created on 22/07/2021
 * Project promptpay-services
 **/

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "partyIdType",
        "partyId"
})
@Generated("jsonschema2pojo")
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MoMoPayee {

    @JsonProperty("partyIdType")
    private EPartyIdType partyIdType;
    @JsonProperty("partyId")
    private String partyId;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("partyIdType")
    public EPartyIdType getPartyIdType() {
        return partyIdType;
    }

    @JsonProperty("partyIdType")
    public void setPartyIdType(EPartyIdType partyIdType) {
        this.partyIdType = partyIdType;
    }

    @JsonProperty("partyId")
    public String getPartyId() {
        return partyId;
    }

    @JsonProperty("partyId")
    public void setPartyId(String partyId) {
        this.partyId = partyId;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public enum EPartyIdType {
        MSISDN
    }
}

package com.credix.cm.promptpay.paymentservice.model;


import com.credix.cm.promptpay.commonsmodel.models.transaction.EPaymentState;
import com.credix.cm.promptpay.commonsmodel.models.transaction.EPaymentType;
import com.credix.cm.promptpay.commonsmodel.models.transaction.TransactionLite;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;
import org.springframework.data.elasticsearch.annotations.Document;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

/**
 * @author Berenger Zina
 * created on 22/07/2021
 * Project promptpay-services
 **/

@Setter
@Getter
@EqualsAndHashCode(callSuper = true)
@Document(indexName = "transaction")
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
public class Transaction extends TransactionLite {
    private TransactionDetails transactionDetails;
    private ExternalTransactionDetails externalTransactionDetails;

    @Builder(builderMethodName = "transactionBuilder")
    public Transaction(TransactionLite transactionLite, TransactionDetails transactionDetails,
                       ExternalTransactionDetails externalTransactionDetails) {
        super(transactionLite);
        this.transactionDetails = transactionDetails;
        this.externalTransactionDetails = externalTransactionDetails;
    }
}

package com.credix.cm.promptpay.paymentservice.factory;

import com.credix.cm.promptpay.commonsmodel.models.transaction.EPaymentState;
import com.credix.cm.promptpay.commonsstream.event.EventType;
import com.credix.cm.promptpay.paymentservice.mobilemoney.momoapi.MoMoApiHelper;
import com.credix.cm.promptpay.paymentservice.model.Transaction;
import com.credix.cm.promptpay.paymentservice.repository.ITransactionRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.util.function.Function;

/**
 * @author beren
 * created on 25/07/2021
 * Project promptpay-services
 **/

@Slf4j
@Component
public class PostTransactionCreationOperatorFactory {
    private final MoMoApiHelper moMoApiHelper;
    private final ITransactionRepository transactionRepository;

    public PostTransactionCreationOperatorFactory(MoMoApiHelper moMoApiHelper, ITransactionRepository transactionRepository) {
        this.moMoApiHelper = moMoApiHelper;
        this.transactionRepository = transactionRepository;
    }

    public Function<Transaction, Mono<Transaction>> getOperationFunction(EventType eventType) {
        switch (eventType) {
            case SUBSCRIPTION_FEE_PAYMENT:
            case CASH_IN_PAYMENT:
                return this.momoCashInFunction();
            case TRANSFER:
                return this.transferFunction();
            default:
                log.warn("No function defined for type {}", eventType);
                throw new UnsupportedOperationException("Operation not handled yet");
        }
    }

    private Function<Transaction, Mono<Transaction>> transferFunction() {
        return Mono::just;
    }

    private Function<Transaction, Mono<Transaction>> momoCashInFunction() {
        return transaction -> moMoApiHelper.doMoMoCashOut(transaction)
                .flatMap(voidResponseEntity -> {
                    if (HttpStatus.ACCEPTED == voidResponseEntity.getStatusCode()) {
                        transaction.setPaymentState(EPaymentState.INITIATED);
                    }
                    return transactionRepository.save(transaction);
                });
    }
}

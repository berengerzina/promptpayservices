package com.credix.cm.promptpay.paymentservice.mobilemoney.momoapi;

import com.credix.cm.promptpay.commonsstream.event.data.PaymentValidationData;
import com.credix.cm.promptpay.paymentservice.exception.PaymentException;
import com.credix.cm.promptpay.paymentservice.mobilemoney.momoapi.model.*;
import com.credix.cm.promptpay.paymentservice.model.Transaction;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.Base64;
import java.util.Objects;
import java.util.function.Predicate;

/**
 * @author Berenger Zina
 * created on 22/07/2021
 * Project promptpay-services
 **/

@Slf4j
@Component
public class MoMoApiHelper {
    private static final Predicate<HttpStatus> errorPredicate = httpStatus -> HttpStatus.ACCEPTED != httpStatus && HttpStatus.OK != httpStatus;
    private final WebClient webClient;
    @Value("${momo.api.host}")
    private String momoApiHost;
    @Value("${momo.api.collection.requestToPay}")
    private String momoRequestToPayApi;
    @Value("${momo.api.collection.token}")
    private String momoGetTokenApi;
    @Value("${momo.api.hostWithoutProtocol}")
    private String hostWithoutProtocol;

    public MoMoApiHelper(WebClient webClient) {
        this.webClient = webClient;
    }

    public Mono<ResponseEntity<Void>> doMoMoCashOut(Transaction transaction) {
        return retrieveMoMoCollectionToken()
                .onStatus(errorPredicate, clientResponse -> {
                    var error = clientResponse.bodyToMono(MoMoApiError.class);
                    return error.map(moMoApiError -> new PaymentException(moMoApiError.getMessage()));
                })
                .bodyToMono(MoMoToken.class)
                .flatMap(moMoToken -> doMoMoRequestToPay(transaction, moMoToken));
    }

    @NotNull
    private WebClient.ResponseSpec retrieveMoMoCollectionToken() {
        log.info("Retrieving Momo Api Token");
        return webClient.post()
                .uri(momoApiHost.concat(momoGetTokenApi))
                .header("Ocp-Apim-Subscription-Key", getMoMoApiCollectionSubscriptionKey())
                .header(HttpHeaders.AUTHORIZATION, getBase64Authorization())
                .header(HttpHeaders.HOST, hostWithoutProtocol)
                .header(HttpHeaders.CONTENT_LENGTH, "0")
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .retrieve();
    }

    @SneakyThrows
    private Mono<ResponseEntity<Void>> doMoMoRequestToPay(Transaction transaction, MoMoToken moMoToken) {
        log.info("Initiating momo payment for transaction {}, amount {}", transaction.getId(), transaction.getAmount());
        return webClient.post()
                .uri(momoApiHost.concat(momoRequestToPayApi))
                .header("Ocp-Apim-Subscription-Key", getMoMoApiCollectionSubscriptionKey())
                .header(HttpHeaders.AUTHORIZATION, "Bearer ".concat(moMoToken.getAccessToken()))
                .header("X-Reference-Id", transaction.getId().toString())
                .header("X-Target-Environment", this.getMomoTargetEnvironment())
                .header(HttpHeaders.HOST, hostWithoutProtocol)
                .body(Mono.just(getMoMoTransferBody(transaction)), MoMoRequestToPayBody.class)
                .retrieve()
                .onStatus(errorPredicate, clientResponse -> {
                    var error = clientResponse.bodyToMono(MoMoApiError.class);
                    return error.map(moMoApiError -> new PaymentException(moMoApiError.getMessage()));
                }).toBodilessEntity();
    }

    @NotNull
    private String getMoMoApiCollectionSubscriptionKey() {
        //ToDo change this
        return "08f31c8b4b6c430a812b4d0dcfc61997";
    }

    private String getMomoTargetEnvironment() {
        //ToDo change this
        return "sandbox";
    }

    private MoMoRequestToPayBody getMoMoTransferBody(Transaction transaction) {
        var momoPayee = MoMoPayee.builder()
                .partyIdType(MoMoPayee.EPartyIdType.MSISDN)
                .partyId(transaction.getTransactionDetails().getPaymentSourceId())
                .build();
        return MoMoRequestToPayBody.builder()
                .amount(transaction.getAmount().intValue())
                .currency(transaction.getTransactionDetails().getDeviseISO())
                .externalId(transaction.getId())
                .payer(momoPayee)
                .payeeNote(transaction.getTransactionDetails().getNote())
                .payerMessage(transaction.getTransactionDetails().getMessage())
                .build();
    }

    private String getBase64Authorization() {
        //ToDo create settings for this
        return "Basic ".concat(Base64.getEncoder().encodeToString(("f657b2a7-5ede-4da5-abc7-1c9e156ea3a7").concat(":").concat("6e56108ff546487f8b15c22b62cfc9ef").getBytes()));
    }

    public Mono<MoMoRequestToPayStatus> validatePayment(PaymentValidationData paymentValidationData) {
        log.info("Validating Momo Payment {}", paymentValidationData.getTransactionId());
        return retrieveMoMoCollectionToken()
                .bodyToMono(MoMoToken.class)
                .flatMap(moMoToken -> webClient.get()
                        .uri(momoApiHost.concat(momoRequestToPayApi).concat("/").concat(paymentValidationData.getTransactionId().toString()))
                        .header("Ocp-Apim-Subscription-Key", getMoMoApiCollectionSubscriptionKey())
                        .header(HttpHeaders.AUTHORIZATION, "Bearer ".concat(moMoToken.getAccessToken()))
                        .header("X-Target-Environment", this.getMomoTargetEnvironment())
                        .header(HttpHeaders.HOST, hostWithoutProtocol)
                        .retrieve()
                        .onStatus(errorPredicate, clientResponse -> {
                            var error = clientResponse.bodyToMono(MoMoApiError.class);
                            return error.map(moMoApiError -> new PaymentException(Objects.nonNull(moMoApiError) ? moMoApiError.getMessage() : ""));
                        })
                        .bodyToMono(MoMoRequestToPayStatus.class)
                        .doOnSuccess(moMoRequestToPayStatus -> {
                            if (MoMoRequestToPayStatus.ERequestTOPayStatus.PENDING == moMoRequestToPayStatus.getStatus()) {
                                log.warn("Transaction {} still in status {} reason {} ", paymentValidationData.getTransactionId(), moMoRequestToPayStatus.getStatus(), moMoRequestToPayStatus.getReason());
                                throw new IllegalStateException("Transaction still pending");
                            }
                        }));
    }
}

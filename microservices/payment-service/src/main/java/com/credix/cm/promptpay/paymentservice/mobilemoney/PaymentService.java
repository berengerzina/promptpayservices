package com.credix.cm.promptpay.paymentservice.mobilemoney;

import com.credix.cm.promptpay.commonsstream.event.DataEvent;
import com.credix.cm.promptpay.commonsstream.event.EventType;
import com.credix.cm.promptpay.commonsstream.event.data.*;
import com.credix.cm.promptpay.commonsstream.response.ErrorResponse;
import com.credix.cm.promptpay.commonsstream.stream.StreamNamesConstant;
import com.credix.cm.promptpay.commonsstream.utils.AbstractStreamBridgeAwareBean;
import com.credix.cm.promptpay.paymentservice.utils.PaymentUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;
import java.util.UUID;

/**
 * @author Berenger Zina
 * created on 22/07/2021
 * Project promptpay-services
 **/

@Slf4j
@Service
public class PaymentService extends AbstractStreamBridgeAwareBean {
    private final PaymentUtils paymentUtils;

    public PaymentService(PaymentUtils paymentUtils) {
        this.paymentUtils = paymentUtils;
    }

    public Mono<Void> processToPayment(DataEvent<UUID, PaymentData> event) {
        return paymentUtils.processToPayment(event)
                .doOnError(throwable -> this.doOnPaymentError(throwable, event))
                .doOnNext(transaction -> {
                    var paymentValidationData = PaymentValidationData.builder()
                            .transactionId(transaction.getId())
                            .originalEventId(event.getData().getOriginalEventId())
                            .originalEventType(event.getEventType())
                            .phoneNumber(event.getData().getPhoneNumber())
                            .build();
                    var dataEvent = new DataEvent<>(UUID.randomUUID(), paymentValidationData,
                            LocalDateTime.now(), EventType.PAYMENT_VALIDATION);
                    this.getStreamBridge().send(StreamNamesConstant.PAYMENT_VALIDATION_STREAM_NAME, dataEvent);
                })
                .thenEmpty(Mono.empty());
    }

    public Mono<Void> validatePayment(DataEvent<UUID, PaymentValidationData> event) {
        return paymentUtils.validatePayment(event.getData())
                .doOnSuccess(transaction -> {
                    if (EventType.SUBSCRIPTION_FEE_PAYMENT == event.getData().getOriginalEventType()) {
                        log.info("Sending subscription payment fee success for subscription {}", event.getData().getOriginalEventId());
                        var subscriptionData = SubscriptionData.builder().payment(transaction).build();
                        var dataEvent = new DataEvent<>(event.getData().getOriginalEventId(), subscriptionData,
                                LocalDateTime.now(), EventType.SUBSCRIPTION_VALIDATION);
                        this.getStreamBridge().send(StreamNamesConstant.SUBSCRIPTION_STREAM_NAME, dataEvent);
                    } else if (EventType.CASH_IN_PAYMENT == event.getData().getOriginalEventType()){
                        log.info("Sending cash in payment success for account  {}", event.getData().getOriginalEventId());
                        var cashInData = CashInData.builder().phoneNumber(event.getData().getPhoneNumber()).payment(transaction).build();
                        var dataEvent = new DataEvent<>(event.getData().getOriginalEventId(), cashInData,
                                LocalDateTime.now(), EventType.CASH_IN_VALIDATION);
                        this.getStreamBridge().send(StreamNamesConstant.CASH_IN_STREAM_NAME, dataEvent);
                    } else if (EventType.TRANSFER == event.getData().getOriginalEventType()){
                        log.info("Sending transfer success for account  {}", event.getData().getOriginalEventId());
                        var transferData = TransferData.builder()
                                .phoneNumber(event.getData().getPhoneNumber())
                                .accountTransfer(transaction)
                                .transferAmount(transaction.getAmount())
                                .sourceAccount(transaction.getSourceAccountNumber())
                                .destinationAccount(transaction.getDestinationAccountNumber())
                                .build();
                        var dataEvent = new DataEvent<>(event.getData().getOriginalEventId(), transferData,
                                LocalDateTime.now(), EventType.TRANSFER_VALIDATION);
                        this.getStreamBridge().send(StreamNamesConstant.TRANSFER_STREAM_NAME, dataEvent);
                    }
                })
                .doOnError(throwable -> this.doOnPaymentValidationError(throwable, event))
                .onErrorStop()
                .thenEmpty(Mono.empty());
    }

    @SneakyThrows
    private void doOnPaymentError(Throwable throwable, DataEvent<UUID, PaymentData> event) {
        log.error("An error occurred during payment processing", throwable);
        var error = ErrorResponse.builder().code(event.getData().getOriginalEventType().name())
                .message("Error during payment processing")
                .build();
        this.sendNotificationEvent(event, new ObjectMapper().writeValueAsString(error));
    }

    @SneakyThrows
    private void doOnPaymentValidationError(Throwable throwable, DataEvent<UUID, PaymentValidationData> event) {
        log.error("An error occurred during payment validation", throwable);
        this.getStreamBridge().send(StreamNamesConstant.PAYMENT_VALIDATION_ERROR_STREAM_NAME, event);
    }
}

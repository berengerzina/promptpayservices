package com.credix.cm.promptpay.paymentservice.utils;

import com.credix.cm.promptpay.commonsmodel.models.transaction.AccountTransfer;
import com.credix.cm.promptpay.commonsmodel.models.transaction.EPaymentState;
import com.credix.cm.promptpay.commonsmodel.models.transaction.TransactionLite;
import com.credix.cm.promptpay.commonsmodel.utils.GenericEntityUtils;
import com.credix.cm.promptpay.commonsstream.event.DataEvent;
import com.credix.cm.promptpay.commonsstream.event.data.PaymentData;
import com.credix.cm.promptpay.commonsstream.event.data.PaymentValidationData;
import com.credix.cm.promptpay.paymentservice.factory.PostTransactionCreationOperatorFactory;
import com.credix.cm.promptpay.paymentservice.factory.TransactionValidatorFactory;
import com.credix.cm.promptpay.paymentservice.model.ExternalTransactionDetails;
import com.credix.cm.promptpay.paymentservice.model.Transaction;
import com.credix.cm.promptpay.paymentservice.model.TransactionDetails;
import com.credix.cm.promptpay.paymentservice.repository.ITransactionRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;
import java.util.UUID;

/**
 * @author Berenger Zina
 * created on 22/07/2021
 * Project promptpay-services
 **/

@Slf4j
@Component
public class PaymentUtils {
    private final ITransactionRepository transactionRepository;
    private final GenericEntityUtils genericEntityUtils;
    private final PostTransactionCreationOperatorFactory postTransactionCreationOperatorFactory;
    private final TransactionValidatorFactory transactionValidatorFactory;

    public PaymentUtils(ITransactionRepository transactionRepository, GenericEntityUtils genericEntityUtils,
                        PostTransactionCreationOperatorFactory postTransactionCreationOperatorFactory, TransactionValidatorFactory transactionValidatorFactory) {
        this.transactionRepository = transactionRepository;
        this.genericEntityUtils = genericEntityUtils;
        this.postTransactionCreationOperatorFactory = postTransactionCreationOperatorFactory;
        this.transactionValidatorFactory = transactionValidatorFactory;
    }

    public Mono<Transaction> processToPayment(DataEvent<UUID, PaymentData> event) {
        log.info("Processing payment event {}", event);
        var transactionOperator = postTransactionCreationOperatorFactory.getOperationFunction(event.getEventType());
        return this.fromPaymentDataToTransactionMono(event.getData())
                .map(transaction -> genericEntityUtils.fillEntityGenericInfos(transaction, event.getKey(), event.getEventDate()))
                .flatMap(transactionRepository::save)
                .flatMap(transactionOperator);
    }

    public Mono<Transaction> fromPaymentDataToTransactionMono(PaymentData paymentData) {
        var accountTransfer = AccountTransfer.builder()
                .paymentDate(LocalDateTime.now())
                .amount(paymentData.getTransactionAmount())
                .destinationAccountId(paymentData.getDestinationAccountId())
                .sourceAccountId(paymentData.getSourceAccountId())
                .sourceAccountNumber(paymentData.getSourceAccountNumber())
                .destinationAccountNumber(paymentData.getDestinationAccountNumber())
                .build();

        var transactionLite = TransactionLite.transactionLiteBuilder()
                .paymentType(paymentData.getPaymentType())
                .paymentState(EPaymentState.CREATED)
                .accountTransfer(accountTransfer)
                .build();
        var transactionDetails = TransactionDetails.builder().paymentMethod(paymentData.getPaymentMethod())
                .deviseISO(paymentData.getDeviseISO())
                .paymentSourceId(paymentData.getPhoneNumber())
                .message(paymentData.getMessage())
                .note(paymentData.getNote())
                .build();

        var transaction = Transaction.transactionBuilder()
                .transactionDetails(transactionDetails)
                .transactionLite(transactionLite)
                .externalTransactionDetails(ExternalTransactionDetails.builder().build())
                .build();
        return Mono.just(transaction);
    }

    public Mono<Transaction> validatePayment(PaymentValidationData paymentValidationData) {
        log.info("Validating payment {} with reason {}", paymentValidationData.getTransactionId(), paymentValidationData.getOriginalEventType());
        return transactionValidatorFactory.getOperationFunction(paymentValidationData.getOriginalEventType())
                .apply(paymentValidationData);
    }
}

package com.credix.cm.promptpay.paymentservice.mobilemoney.momoapi.model;

import com.fasterxml.jackson.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.annotation.Generated;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * @author Berenger Zina
 * created on 22/07/2021
 * Project promptpay-services
 **/

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "amount",
        "currency",
        "externalId",
        "payee",
        "payerMessage",
        "payeeNote"
})
@Generated("jsonschema2pojo")
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MoMoRequestToPayBody implements Serializable {

    @JsonProperty("amount")
    private Integer amount;
    @JsonProperty("currency")
    private String currency;
    @JsonProperty("externalId")
    private UUID externalId;
    @JsonProperty("payer")
    private MoMoPayee payer;
    @JsonProperty("payerMessage")
    private String payerMessage;
    @JsonProperty("payeeNote")
    private String payeeNote;

    @JsonProperty("amount")
    public Integer getAmount() {
        return amount;
    }

    @JsonProperty("amount")
    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    @JsonProperty("currency")
    public String getCurrency() {
        return currency;
    }

    @JsonProperty("currency")
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @JsonProperty("externalId")
    public UUID getExternalId() {
        return externalId;
    }

    @JsonProperty("externalId")
    public void setExternalId(UUID externalId) {
        this.externalId = externalId;
    }

    @JsonProperty("payer")
    public MoMoPayee getPayee() {
        return payer;
    }

    @JsonProperty("payer")
    public void setPayee(MoMoPayee payee) {
        this.payer = payee;
    }

    @JsonProperty("payerMessage")
    public String getPayerMessage() {
        return payerMessage;
    }

    @JsonProperty("payerMessage")
    public void setPayerMessage(String payerMessage) {
        this.payerMessage = payerMessage;
    }

    @JsonProperty("payeeNote")
    public String getPayeeNote() {
        return payeeNote;
    }

    @JsonProperty("payeeNote")
    public void setPayeeNote(String payeeNote) {
        this.payeeNote = payeeNote;
    }



}

package com.credix.cm.promptpay.paymentservice.exception;

import com.credix.cm.promptpay.commonsmodel.models.generic.GenericEntity;
import com.credix.cm.promptpay.commonsutil.exception.GenericException;

/**
 * @author Berenger Zina
 * created on 22/07/2021
 * Project promptpay-services
 **/

public class PaymentException extends GenericException {
    public PaymentException() {
    }

    public PaymentException(String message) {
        super(message);
    }

    @Override
    public <T extends GenericEntity> T getEntity() {
        return null;
    }
}

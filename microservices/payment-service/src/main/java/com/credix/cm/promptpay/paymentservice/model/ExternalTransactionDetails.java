package com.credix.cm.promptpay.paymentservice.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author beren
 * created on 25/07/2021
 * Project promptpay-services
 **/

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ExternalTransactionDetails implements Serializable {
    private String financialTransactionId;
    private String errorCode;
}

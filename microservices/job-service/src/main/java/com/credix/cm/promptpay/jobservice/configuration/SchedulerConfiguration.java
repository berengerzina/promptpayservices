package com.credix.cm.promptpay.jobservice.configuration;

import com.credix.cm.promptpay.jobservice.commons.Constants;
import com.credix.cm.promptpay.jobservice.entity.JobParameters;
import com.credix.cm.promptpay.jobservice.listener.JobTerminatedListener;
import com.credix.cm.promptpay.jobservice.repository.IJobParametersRepository;
import lombok.extern.slf4j.Slf4j;
import org.quartz.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.scheduling.quartz.SpringBeanJobFactory;

import javax.sql.DataSource;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static org.quartz.CronScheduleBuilder.cronSchedule;
import static org.quartz.JobBuilder.newJob;
import static org.quartz.TriggerBuilder.newTrigger;

@Slf4j
@Configuration
public class SchedulerConfiguration {

    @Bean
    public List<Trigger> triggers(IJobParametersRepository jobParametersRepository) {
        return jobParametersRepository.findAll().stream()
                .map(jobParameters -> {
                    log.info("Configuring trigger for Job {} with cron {}", jobParameters.getCode(), jobParameters.getCronExpression());
                    try {
                        return newTrigger().forJob(jobDetail(jobParameters))
                                .withIdentity(TriggerKey.triggerKey(jobParameters.getCode()))
                                .withDescription(jobParameters.getCronDescription())
                                .withSchedule(cronSchedule(jobParameters.getCronExpression()))
                                .startNow()
                                .build();
                    } catch (Exception e) {
                        log.error("An error occured during Trigger initialization {}", jobParameters.getId(), e);
                    }
                    return null;
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    @Bean
    public List<JobDetail> jobDetails(IJobParametersRepository jobParametersRepository) {
        return jobParametersRepository.findAll().stream()
                .map(jobParameters -> {
                    log.info("Configuring JobDetail for Job {}", jobParameters.getCode());
                    try {
                        return jobDetail(jobParameters);
                    } catch (Exception e) {
                        log.error("An error occured during JobDetail initialization {}", jobParameters.getId(), e);
                    }
                    return null;
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    public JobDetail jobDetail(JobParameters jobParameters) throws ClassNotFoundException {
        JobDataMap jobDataMap = new JobDataMap();
        jobDataMap.put(Constants.JOB_CODE, jobParameters.getCode());
        return newJob()
                .ofType((Class<? extends Job>) Class.forName(jobParameters.getClassName()))
                .storeDurably()
                .withIdentity(JobKey.jobKey(jobParameters.getCode()))
                .withDescription(jobParameters.getDescription())
                .setJobData(jobDataMap)
                .build();
    }

    @Bean
    public SchedulerFactoryBean scheduler(List<Trigger> triggers, List<JobDetail> jobDetails, @Qualifier("quartzDataSource") DataSource dataSource,
                                          JobTerminatedListener jobTerminatedListener, SpringBeanJobFactory springBeanJobFactory) {
        var schedulerFactory = new SchedulerFactoryBean();
        schedulerFactory.setConfigLocation(new ClassPathResource("quartz.properties"));
        schedulerFactory.setJobFactory(springBeanJobFactory);

        log.debug("Setting the Scheduler up");
        schedulerFactory.setJobDetails(jobDetails.toArray(new JobDetail[0]));
        schedulerFactory.setTriggers(triggers.toArray(new Trigger[0]));
        schedulerFactory.setGlobalTriggerListeners(jobTerminatedListener);

        // Comment the following line to use the default Quartz job store.
        schedulerFactory.setDataSource(dataSource);
        schedulerFactory.start();

        return schedulerFactory;
    }

    @Bean
    public SpringBeanJobFactory springBeanJobFactory(ApplicationContext applicationContext) {
        var jobFactory = new AutoWiringSpringBeanJobFactory(applicationContext.getAutowireCapableBeanFactory());
        log.debug("Configuring Job factory");
        return jobFactory;
    }
}

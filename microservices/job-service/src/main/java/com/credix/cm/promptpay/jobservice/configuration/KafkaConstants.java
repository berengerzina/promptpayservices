package com.credix.cm.promptpay.jobservice.configuration;

import com.credix.cm.promptpay.commonsstream.stream.StreamNamesConstant;

/**
 * @author beren
 * created on 26/07/2021
 * Project promptpay-services
 **/
public class KafkaConstants {
    public static final String KAFKA_BROKERS = "localhost:9092";

    public static final String CLIENT_ID="job-service";

    public static final String GROUP_ID_CONFIG="paymentValidationError-group";

    public static final String OFFSET_RESET_EARLIER="earliest";

    public static String OFFSET_RESET_LATEST="latest";

    public static final Integer MAX_POLL_RECORDS=20;
}

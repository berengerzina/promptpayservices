package com.credix.cm.promptpay.jobservice.job.generic;

import com.credix.cm.promptpay.commonsstream.event.DataEvent;
import com.credix.cm.promptpay.commonsstream.event.data.AbstractData;
import com.credix.cm.promptpay.jobservice.commons.Constants;
import com.credix.cm.promptpay.jobservice.configuration.KafkaConstants;
import com.credix.cm.promptpay.jobservice.deserializer.CustomDeserializer;
import com.credix.cm.promptpay.jobservice.repository.IJobParametersRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.LongDeserializer;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Properties;

/**
 * @author beren
 * created on 26/07/2021
 * Project promptpay-services
 **/
@Slf4j
public abstract class GenericKafkaTopicJob<K, T extends AbstractData> implements Job {

    private final Properties props = new Properties();
    protected Consumer<Long, DataEvent<K, T>> consumer;
    protected String jobCode;
    private IJobParametersRepository jobParametersRepository;

    {
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, KafkaConstants.KAFKA_BROKERS);
        props.put(ConsumerConfig.GROUP_ID_CONFIG, KafkaConstants.GROUP_ID_CONFIG);
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, LongDeserializer.class.getName());
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, this.getDeserializerClass().getName());
        props.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, KafkaConstants.MAX_POLL_RECORDS);
        props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "false");
        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, KafkaConstants.OFFSET_RESET_LATEST);
    }

    public abstract void executeJob();

    @Override
    public void execute(JobExecutionContext context) {
        jobCode = context.getMergedJobDataMap().get(Constants.JOB_CODE).toString();
        consumer = this.createConsumer();
        this.executeJob();
        consumer.commitAsync();
        consumer.close();
    }

    @Transactional
    public LocalDateTime retrieveLastExecutionDate() {
        return this.jobParametersRepository.findLastExecutionTimeForJob(jobCode);
    }

    @Autowired
    public void setJobParametersRepository(IJobParametersRepository jobParametersRepository) {
        this.jobParametersRepository = jobParametersRepository;
    }

    private Consumer<Long, DataEvent<K, T>> createConsumer() {
        Consumer<Long, DataEvent<K, T>> consumer = new KafkaConsumer<>(props);
        consumer.subscribe(Collections.singletonList(this.getTopicName()));
        return consumer;
    }

    protected abstract String getTopicName();

    protected abstract Class<? extends CustomDeserializer> getDeserializerClass();
}

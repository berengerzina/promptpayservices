package com.credix.cm.promptpay.jobservice.listener;

import com.credix.cm.promptpay.jobservice.repository.IJobParametersRepository;
import lombok.extern.slf4j.Slf4j;
import org.quartz.JobExecutionContext;
import org.quartz.Trigger;
import org.quartz.TriggerListener;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.time.LocalDateTime;

@Slf4j
@Component
public class JobTerminatedListener implements TriggerListener {
    private IJobParametersRepository jobParametersRepository;

    public JobTerminatedListener(IJobParametersRepository jobParametersRepository) {
        this.jobParametersRepository = jobParametersRepository;
    }

    @Override
    public String getName() {
        return "JobTerminatedListener";
    }

    @Override
    @Transactional
    public void triggerFired(Trigger trigger, JobExecutionContext jobExecutionContext) {
        jobParametersRepository.findJobParametersByCode(trigger.getJobKey().getName()).stream().forEach(jobParameters -> {
            jobParameters.setLastFireTime(LocalDateTime.now());
            jobParametersRepository.save(jobParameters);
        });
    }

    @Override
    public boolean vetoJobExecution(Trigger trigger, JobExecutionContext jobExecutionContext) {
        return false;
    }

    @Override
    public void triggerMisfired(Trigger trigger) {
        log.warn("Job in group {} with name {} was not fired/executed", trigger.getJobKey().getGroup(), trigger.getJobKey().getName());
        jobParametersRepository.findJobParametersByCode(trigger.getJobKey().getName()).stream().forEach(jobParameters -> {
            jobParameters.setLastExecutionSuccess(false);
            jobParametersRepository.save(jobParameters);
        });
    }

    @Override
    public void triggerComplete(Trigger trigger, JobExecutionContext jobExecutionContext, Trigger.CompletedExecutionInstruction completedExecutionInstruction) {
        log.info("Job in group {} with name {} was fired/executed successfully", trigger.getJobKey().getGroup(), trigger.getJobKey().getName());
        jobParametersRepository.findJobParametersByCode(trigger.getJobKey().getName()).stream().forEach(jobParameters -> {
            jobParameters.setLastExecutionSuccessTime(jobParameters.getLastFireTime());
            jobParameters.setLastExecutionSuccess(true);
            jobParametersRepository.save(jobParameters);
        });
    }

}

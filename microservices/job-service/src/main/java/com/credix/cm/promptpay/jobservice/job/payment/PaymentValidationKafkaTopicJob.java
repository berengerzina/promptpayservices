package com.credix.cm.promptpay.jobservice.job.payment;

import com.credix.cm.promptpay.commonsstream.event.DataEvent;
import com.credix.cm.promptpay.commonsstream.event.data.PaymentValidationData;
import com.credix.cm.promptpay.commonsstream.stream.StreamNamesConstant;
import com.credix.cm.promptpay.jobservice.deserializer.PaymentValidationDeserializer;
import com.credix.cm.promptpay.jobservice.job.generic.GenericKafkaTopicJob;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.function.StreamBridge;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.util.UUID;

/**
 * @author beren
 * created on 26/07/2021
 * Project promptpay-services
 **/
@Slf4j
@Component
public class PaymentValidationKafkaTopicJob extends GenericKafkaTopicJob<UUID, PaymentValidationData> {
    private StreamBridge streamBridge;

    @Override
    public void executeJob() {
        log.info("Starting replaying payment validation event");
        ConsumerRecords<Long, DataEvent<UUID, PaymentValidationData>> consumerRecords = consumer.poll(Duration.ofMillis(100));
        log.info("There are {} events to be replayed!", consumerRecords.count());
        consumerRecords.forEach(longDataEventConsumerRecord -> {
            log.info("Message read id {} value {}", longDataEventConsumerRecord.key(), longDataEventConsumerRecord.value());
            if (longDataEventConsumerRecord.value().getData().getNumberOfRetry() <= 3) {
                longDataEventConsumerRecord.value().getData().setNumberOfRetry(longDataEventConsumerRecord.value().getData().getNumberOfRetry() + 1);
                streamBridge.send(StreamNamesConstant.PAYMENT_VALIDATION_STREAM_NAME, longDataEventConsumerRecord.value());
            }
        });
    }

    @Override
    protected String getTopicName() {
        return StreamNamesConstant.PAYMENT_VALIDATION_ERROR_STREAM_NAME;
    }

    @Override
    protected Class<PaymentValidationDeserializer> getDeserializerClass() {
        return PaymentValidationDeserializer.class;
    }

    @Autowired
    public void setStreamBridge(StreamBridge streamBridge) {
        this.streamBridge = streamBridge;
    }
}

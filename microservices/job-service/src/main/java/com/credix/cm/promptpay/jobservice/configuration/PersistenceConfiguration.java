package com.credix.cm.promptpay.jobservice.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.quartz.QuartzDataSource;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.sql.DataSource;

/**
 * @author beren
 * created on 26/07/2021
 * Project promptpay-services
 **/
@Configuration
public class PersistenceConfiguration {
    @Value("${quartz.datasource.url}")
    private String quartzDbUrl;
    @Value("${quartz.datasource.username}")
    private String quartzDbUsername;
    @Value("${quartz.datasource.password}")
    private String quartzDbPassword;
    @Value("${quartz.datasource.driver.className}")
    private String quartzDriverClassName;

    @Value("${spring.datasource.url}")
    private String serviceDbUrl;
    @Value("${spring.datasource.username}")
    private String serviceDbUsername;
    @Value("${spring.datasource.password}")
    private String serviceDbPassword;
    @Value("${spring.datasource.driver.className}")
    private String serviceDriverClassName;

    @Bean
    @Primary
    public DataSource dataSource(){
        return DataSourceBuilder.create()
                .url(serviceDbUrl)
                .driverClassName(serviceDriverClassName)
                .username(serviceDbUsername)
                .password(serviceDbPassword)
                .build();
    }

    @Bean("quartzDataSource")
    @QuartzDataSource
    public DataSource quartzDataSource() {
        return DataSourceBuilder.create()
                .url(quartzDbUrl)
                .username(quartzDbUsername)
                .password(quartzDbPassword)
                .driverClassName(quartzDriverClassName)
                .build();
    }
}

package com.credix.cm.promptpay.jobservice.repository;

import com.credix.cm.promptpay.jobservice.entity.JobParameters;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface IJobParametersRepository extends JpaRepository<JobParameters, UUID> {
    Optional<JobParameters> findJobParametersByCode(String code);

    @Query("SELECT jobParam.lastExecutionSuccessTime FROM JobParameters jobParam WHERE jobParam.code = ?1")
    LocalDateTime findLastExecutionTimeForJob(String jobCode);
}

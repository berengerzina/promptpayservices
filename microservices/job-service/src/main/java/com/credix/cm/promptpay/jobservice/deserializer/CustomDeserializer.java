package com.credix.cm.promptpay.jobservice.deserializer;

import com.credix.cm.promptpay.commonsstream.event.DataEvent;
import com.credix.cm.promptpay.commonsstream.event.data.AbstractData;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.serialization.Deserializer;

import java.util.Map;

@Slf4j
public abstract class CustomDeserializer<K, T extends AbstractData> implements Deserializer<DataEvent<K, T>> {
    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {
    }

    @Override
    public DataEvent<K, T> deserialize(String topic, byte[] data) {
        var mapper = new ObjectMapper();
        DataEvent<K, T> event = null;
        try {
            event = mapper.readValue(data, this.getTypeReference());
        } catch (Exception exception) {
            log.error("Error in deserializing bytes ", exception);
        }
        return event;
    }

    protected abstract TypeReference<DataEvent<K, T>> getTypeReference();

    @Override
    public void close() {
    }
}
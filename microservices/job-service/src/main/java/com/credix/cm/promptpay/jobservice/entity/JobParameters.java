package com.credix.cm.promptpay.jobservice.entity;

import com.credix.cm.promptpay.commonsjpa.model.entity.GenericEntity;
import lombok.*;
import org.hibernate.annotations.Where;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Table(name = "T_JOB_PARAMETERS")
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(of = "id")
@Builder
@AllArgsConstructor
@Where(clause = "BOO_ACTIVE = true")
public class JobParameters extends GenericEntity {
    @Column(name = "CODE")
    private String code;
    @Column(name = "DESCRIPTION")
    private String description;
    @Column(name = "CLASS_NAME")
    private String className;
    @Column(name = "CRON_EXPRESSION")
    private String cronExpression;
    @Column(name = "CRON_DESCRIPTION")
    private String cronDescription;
    @Column(name = "LAST_FIRE_TIME")
    private LocalDateTime lastFireTime;
    @Column(name = "BOO_LAST_EXEC_SUCCESS")
    private boolean lastExecutionSuccess;
    @Column(name = "LAST_EXEC_SUCCESS_TIME")
    private LocalDateTime lastExecutionSuccessTime;
    @Column(name = "BOO_ACTIVE")
    private boolean active;
}

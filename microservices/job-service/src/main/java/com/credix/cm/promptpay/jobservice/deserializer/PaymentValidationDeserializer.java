package com.credix.cm.promptpay.jobservice.deserializer;

import com.credix.cm.promptpay.commonsstream.event.DataEvent;
import com.credix.cm.promptpay.commonsstream.event.data.PaymentValidationData;
import com.fasterxml.jackson.core.type.TypeReference;

import java.util.UUID;

/**
 * @author beren
 * created on 27/07/2021
 * Project promptpay-services
 **/
public class PaymentValidationDeserializer extends CustomDeserializer<UUID, PaymentValidationData> {
    @Override
    protected TypeReference<DataEvent<UUID, PaymentValidationData>> getTypeReference() {
        return new TypeReference<DataEvent<UUID, PaymentValidationData>>() {};
    }
}

DO
$$
    BEGIN
        CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

        INSERT INTO T_JOB_PARAMETERS(id, CODE, DESCRIPTION, CRON_EXPRESSION, CRON_DESCRIPTION, CLASS_NAME)
        values (uuid_generate_v4(), 'PAYMENT_VALIDATION_REPLAY_JOB', 'Job to replay payment validation events that failed',
                '0/30 0/1 * 1/1 * ? *', 'Executing every 30 seconds',
                'com.credix.cm.promptpay.jobservice.job.payment.PaymentValidationKafkaTopicJob');
    END;
$$
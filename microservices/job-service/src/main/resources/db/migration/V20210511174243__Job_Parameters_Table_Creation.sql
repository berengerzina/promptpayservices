DO
$$
    BEGIN
        CREATE TABLE IF NOT EXISTS T_JOB_PARAMETERS
        (
            ID                     uuid      NOT NULL PRIMARY KEY,
            CODE                   varchar   not null unique,
            DESCRIPTION            varchar,
            CRON_EXPRESSION        varchar   not null,
            CRON_DESCRIPTION       varchar,
            CLASS_NAME             varchar   not null,
            LAST_FIRE_TIME         timestamp          default '1970-01-01 19:10:25-07',
            BOO_LAST_EXEC_SUCCESS  boolean            default true,
            LAST_EXEC_SUCCESS_TIME timestamp          default '1970-01-01 19:10:25-07',
            CREATED_ON             timestamp not null default current_timestamp,
            UPDATED_ON             timestamp,
            BOO_DELETED            boolean            default false,
            BOO_ACTIVE             boolean            default true
        );
    END;
$$
package com.credix.cm.promptpay.dataflowservice;

import com.credix.cm.promptpay.dataflowservice.handler.RequestHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;

import static org.springframework.web.reactive.function.BodyInserters.fromValue;
import static org.springframework.web.reactive.function.server.RequestPredicates.GET;
import static org.springframework.web.reactive.function.server.RequestPredicates.POST;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;
import static org.springframework.web.reactive.function.server.ServerResponse.ok;

/**
 * @author Berenger Zina
 * created on 22/07/2021
 * Project promptpay-services
 **/

@Slf4j
@SpringBootApplication
public class DataflowServiceApplication {

    @Autowired
    private RequestHandler requestHandler;

    public static void main(String[] args) {
        SpringApplication.run(DataflowServiceApplication.class, args);
    }

    @Bean
    public RouterFunction<ServerResponse> routingFunction(){
        return route(GET("/home"), serverRequest -> ok().body(fromValue("works!")))
                .andRoute(GET("/promptpay/status/{account}"), requestHandler::refreshAccountStatus)
                .andRoute(GET("/promptpay/configure/{phone}"), requestHandler::configure)
                .andRoute(POST("/promptpay/subscription"), requestHandler::subscription)
                .andRoute(POST("/promptpay/cashIn"), requestHandler::cashIn)
                .andRoute(POST("/promptpay/cashOut"), requestHandler::cashOut)
                .andRoute(POST("/promptpay/transfer"), requestHandler::transfer)
                .filter((request, next) -> {
                    log.info("Before handler invocation: " + request.path());
                    return next.handle(request);
                });
    }

}

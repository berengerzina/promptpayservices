package com.credix.cm.promptpay.dataflowservice.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Berenger Zina
 * created on 22/07/2021
 * Project promptpay-services
 **/

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DataRequest {
    private String sentTimeStamp;
    private String data;
}

package com.credix.cm.promptpay.dataflowservice.message;

import com.credix.cm.promptpay.commonsstream.event.DataEvent;
import com.credix.cm.promptpay.commonsstream.event.EventType;
import com.credix.cm.promptpay.commonsstream.event.data.CashInData;
import com.credix.cm.promptpay.commonsstream.event.data.SubscriptionData;
import com.credix.cm.promptpay.commonsstream.event.data.TransferData;
import com.credix.cm.promptpay.commonsstream.stream.StreamNamesConstant;
import com.credix.cm.promptpay.dataflowservice.model.CashInDataBody;
import com.credix.cm.promptpay.dataflowservice.model.RefreshDataBody;
import com.credix.cm.promptpay.dataflowservice.model.SubscriptionDataBody;
import com.credix.cm.promptpay.dataflowservice.model.TransferDataBody;
import org.springframework.cloud.stream.function.StreamBridge;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;
import java.util.UUID;

/**
 * @author Berenger Zina
 * created on 22/07/2021
 * Project promptpay-services
 **/

@Component
public class MessageProducer {
    private final StreamBridge streamBridge;

    public MessageProducer(StreamBridge streamBridge) {
        this.streamBridge = streamBridge;
    }

    public Mono<Void> sendSubscriptionEvent(SubscriptionDataBody subscriptionDataBody){
        var subscriptionData = SubscriptionData.builder()
                .clientAddress(subscriptionDataBody.getClientAddress())
                .phoneNumber(subscriptionDataBody.getPhoneNumber())
                .clientBirthdate(subscriptionDataBody.getClientBirthdate())
                .clientCNIExpiration(subscriptionDataBody.getClientCNIExpiration())
                .clientCNINumber(subscriptionDataBody.getClientCNINumber())
                .clientName(subscriptionDataBody.getClientName())
                .paymentMethod(subscriptionDataBody.getPaymentMethod())
                .deviseISO(subscriptionDataBody.getDeviseISO())
                .build();
        return Mono.just(subscriptionData)
                .map(subscriptionData1 -> new DataEvent<>(UUID.randomUUID(), subscriptionData1, LocalDateTime.now(), EventType.SUBSCRIPTION))
                .doOnNext(dataEvent -> streamBridge.send(StreamNamesConstant.SUBSCRIPTION_STREAM_NAME, dataEvent))
                .thenEmpty(Mono.empty());
    }

    public Mono<Void> sendPaymentEvent(CashInDataBody cashInDataBody){
        var cashInData = CashInData.builder()
                .phoneNumber(cashInDataBody.getPhoneNumber())
                .cashInAmount(cashInDataBody.getCashInAmount())
                .build();
        return Mono.just(cashInData)
                .map(cashInData1 -> new DataEvent<>(UUID.randomUUID(), cashInData1, LocalDateTime.now(), EventType.CASH_IN))
                .doOnNext(dataEvent -> streamBridge.send(StreamNamesConstant.CASH_IN_STREAM_NAME, dataEvent))
                .thenEmpty(Mono.empty());
    }

    public Mono<Void> sendRefreshEvent(RefreshDataBody refreshDataBody) {
        return Mono.empty();
    }

    public Mono<Void> sendTransferEvent(TransferDataBody transferDataBody) {
        var transferData = TransferData.builder()
                .phoneNumber(transferDataBody.getPhoneNumber())
                .transferAmount(transferDataBody.getTransferAmount())
                .sourceAccount(transferDataBody.getSourceAccount())
                .destinationAccount(transferDataBody.getDestinationAccount())
                .build();
        return Mono.just(transferData)
                .map(transferData1 -> new DataEvent<>(UUID.randomUUID(), transferData1, LocalDateTime.now(), EventType.TRANSFER))
                .doOnNext(dataEvent -> streamBridge.send(StreamNamesConstant.TRANSFER_STREAM_NAME, dataEvent))
                .thenEmpty(Mono.empty());
    }
}

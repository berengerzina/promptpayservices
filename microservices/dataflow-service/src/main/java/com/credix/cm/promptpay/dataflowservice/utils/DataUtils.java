package com.credix.cm.promptpay.dataflowservice.utils;

import com.credix.cm.promptpay.dataflowservice.model.AbstractDataBody;
import com.credix.cm.promptpay.dataflowservice.model.DataRequest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author Berenger Zina
 * created on 22/07/2021
 * Project promptpay-services
 **/

@Slf4j
@Component
public class DataUtils {
    private final ObjectMapper objectMapper;

    public DataUtils(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    public <T extends AbstractDataBody> T extractDataObjectFromDataRequest(DataRequest dataRequest, Class<T> aClass) {
        try {
            return objectMapper.readValue(dataRequest.getData(), aClass);
        } catch (JsonProcessingException e) {
            log.error("Error converting data {} to class {}", dataRequest.getData(), aClass);
            throw new RuntimeException(e);
        }
    }
}

package com.credix.cm.promptpay.dataflowservice.model;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

/**
 * @author beren
 * created on 23/07/2021
 * Project promptpay-services
 **/

@Getter
@Setter
@EqualsAndHashCode(of = {"sourceAccount", "destinationAccount", "transferAmount"}, callSuper = true)
public class TransferDataBody extends AbstractDataBody {
    private String sourceAccount;
    private String destinationAccount;
    private BigDecimal transferAmount;

    @Builder
    public TransferDataBody(String phoneNumber, String sourceAccount, String destinationAccount, BigDecimal transferAmount) {
        super(phoneNumber);
        this.sourceAccount = sourceAccount;
        this.destinationAccount = destinationAccount;
        this.transferAmount = transferAmount;
    }
}

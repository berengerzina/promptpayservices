package com.credix.cm.promptpay.dataflowservice.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Berenger Zina
 * created on 22/07/2021
 * Project promptpay-services
 **/

@Getter
@Setter
public class RefreshDataBody extends AbstractDataBody {
    @Builder
    public RefreshDataBody(String phoneNumber) {
        super(phoneNumber);
    }
}

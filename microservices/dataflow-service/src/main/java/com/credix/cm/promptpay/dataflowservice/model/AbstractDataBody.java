package com.credix.cm.promptpay.dataflowservice.model;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author Berenger Zina
 * created on 22/07/2021
 * Project promptpay-services
 **/

@Data
@AllArgsConstructor
public abstract class AbstractDataBody {
    private String phoneNumber;
}

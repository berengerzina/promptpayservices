package com.credix.cm.promptpay.dataflowservice.handler;

import com.credix.cm.promptpay.dataflowservice.message.MessageProducer;
import com.credix.cm.promptpay.dataflowservice.model.*;
import com.credix.cm.promptpay.dataflowservice.utils.DataUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

/**
 * @author Berenger Zina
 * created on 22/07/2021
 * Project promptpay-services
 **/

@Component
public class RequestHandler {
    private final DataUtils dataUtils;
    private final MessageProducer messageProducer;

    public RequestHandler(DataUtils dataUtils, MessageProducer messageProducer) {
        this.dataUtils = dataUtils;
        this.messageProducer = messageProducer;
    }

    public Mono<ServerResponse> refreshAccountStatus(ServerRequest serverRequest) {
        return serverRequest.bodyToMono(DataRequest.class)
                .map(dataRequest -> dataUtils.extractDataObjectFromDataRequest(dataRequest, RefreshDataBody.class))
                .doOnNext(refreshDataBody -> messageProducer.sendRefreshEvent(refreshDataBody).subscribe())
                .flatMap(refreshDataBody -> ServerResponse.accepted().build());
    }

    public Mono<ServerResponse> subscription(ServerRequest serverRequest) {
        return serverRequest.bodyToMono(DataRequest.class)
                .map(dataRequest -> dataUtils.extractDataObjectFromDataRequest(dataRequest, SubscriptionDataBody.class))
                .doOnNext(subscriptionDataBody -> messageProducer.sendSubscriptionEvent(subscriptionDataBody).subscribe())
                .flatMap(subscriptionDataBody -> ServerResponse.accepted().build());
    }

    public Mono<ServerResponse> cashIn(ServerRequest serverRequest) {
        return serverRequest.bodyToMono(DataRequest.class)
                .map(dataRequest -> dataUtils.extractDataObjectFromDataRequest(dataRequest, CashInDataBody.class))
                .doOnNext(cashInDataBody -> messageProducer.sendPaymentEvent(cashInDataBody).subscribe())
                .flatMap(cashInDataBody -> ServerResponse.accepted().build());
    }

    public Mono<ServerResponse> cashOut(ServerRequest serverRequest) {
        return Mono.empty();
    }

    public Mono<ServerResponse> transfer(ServerRequest serverRequest) {
        return serverRequest.bodyToMono(DataRequest.class)
                .map(dataRequest -> dataUtils.extractDataObjectFromDataRequest(dataRequest, TransferDataBody.class))
                .doOnNext(transferDataBody -> messageProducer.sendTransferEvent(transferDataBody).subscribe())
                .flatMap(cashInDataBody -> ServerResponse.accepted().build());
    }

    public Mono<ServerResponse> configure(ServerRequest serverRequest) {
        return Mono.empty();
    }
}

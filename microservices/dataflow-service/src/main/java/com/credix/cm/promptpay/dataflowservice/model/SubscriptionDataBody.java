package com.credix.cm.promptpay.dataflowservice.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

/**
 * @author Berenger Zina
 * created on 22/07/2021
 * Project promptpay-services
 **/

@Getter
@Setter
public class SubscriptionDataBody extends AbstractDataBody {
    private String clientName;
    private LocalDate clientBirthdate;
    private String clientAddress;
    private String clientCNINumber;
    private LocalDate clientCNIExpiration;
    private String deviseISO;
    private String paymentMethod;

    @Builder
    public SubscriptionDataBody(String phoneNumber, String clientName, LocalDate clientBirthdate, String clientAddress,
                                String clientCNINumber, LocalDate clientCNIExpiration, String deviseISO, String paymentMethod) {
        super(phoneNumber);
        this.clientName = clientName;
        this.clientBirthdate = clientBirthdate;
        this.clientAddress = clientAddress;
        this.clientCNINumber = clientCNINumber;
        this.clientCNIExpiration = clientCNIExpiration;
        this.deviseISO = deviseISO;
        this.paymentMethod = paymentMethod;
    }
}

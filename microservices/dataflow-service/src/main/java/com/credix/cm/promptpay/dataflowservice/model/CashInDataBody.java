package com.credix.cm.promptpay.dataflowservice.model;

import com.credix.cm.promptpay.commonsstream.event.EventType;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.UUID;

/**
 * @author beren
 * created on 23/07/2021
 * Project promptpay-services
 **/
@Getter
@Setter
public class CashInDataBody extends AbstractDataBody {
    private BigDecimal cashInAmount;

    @Builder
    public CashInDataBody(String phoneNumber, BigDecimal cashInAmount) {
        super(phoneNumber);
        this.cashInAmount = cashInAmount;
    }
}

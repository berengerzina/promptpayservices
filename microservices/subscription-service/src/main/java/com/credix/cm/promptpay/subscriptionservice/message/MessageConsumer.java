package com.credix.cm.promptpay.subscriptionservice.message;

import com.credix.cm.promptpay.commonsstream.event.DataEvent;
import com.credix.cm.promptpay.commonsstream.event.data.SubscriptionData;
import com.credix.cm.promptpay.subscriptionservice.service.SubscriptionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.UUID;
import java.util.function.Consumer;

/**
 * @author Berenger Zina
 * created on 22/07/2021
 * Project promptpay-services
 **/

@Slf4j
@Component
public class MessageConsumer {
    private SubscriptionService subscriptionService;

    public MessageConsumer(SubscriptionService subscriptionService) {
        this.subscriptionService = subscriptionService;
    }

    @Bean
    public Consumer<DataEvent<UUID, SubscriptionData>> subscriptionConsumer() {
        return event -> {
            log.info("Inbound message type {}, date {}, data {}", event.getEventType(), event.getEventDate(), event.getData());
            switch (event.getEventType()){
                case SUBSCRIPTION:
                    subscriptionService.createSubscription(event).subscribe();
                    break;
                case SUBSCRIPTION_VALIDATION:
                    subscriptionService.validateSubscription(event).subscribe();
                    break;
                default:
                    throw new UnsupportedOperationException("event handler not present for this consumer");
            }
        };
    }
}

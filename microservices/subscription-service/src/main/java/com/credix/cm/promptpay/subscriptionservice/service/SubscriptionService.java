package com.credix.cm.promptpay.subscriptionservice.service;

import com.credix.cm.promptpay.commonsmodel.utils.GenericEntityUtils;
import com.credix.cm.promptpay.commonsstream.event.DataEvent;
import com.credix.cm.promptpay.commonsstream.event.data.SubscriptionData;
import com.credix.cm.promptpay.subscriptionservice.repository.ISubscriptionRepository;
import com.credix.cm.promptpay.subscriptionservice.util.SubscriptionUtils;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.UUID;

/**
 * @author Berenger Zina
 * created on 22/07/2021
 * Project promptpay-services
 **/

@Service
public class SubscriptionService {
    private final SubscriptionUtils subscriptionUtils;
    private final GenericEntityUtils genericEntityUtils;
    private final ISubscriptionRepository subscriptionRepository;

    public SubscriptionService(SubscriptionUtils subscriptionUtils, GenericEntityUtils genericEntityUtils, ISubscriptionRepository subscriptionRepository) {
        this.subscriptionUtils = subscriptionUtils;
        this.genericEntityUtils = genericEntityUtils;
        this.subscriptionRepository = subscriptionRepository;
    }

    public Mono<Void> createSubscription(DataEvent<UUID, SubscriptionData> dataEvent) {
        return subscriptionUtils.fromSubscriptionDataToSubscriptionMono(dataEvent.getData())
                .map(subscription -> genericEntityUtils.fillEntityGenericInfos(subscription, dataEvent.getKey(), dataEvent.getEventDate()))
                .doOnNext(subscriptionUtils::validateSubscription)
                .doOnError(throwable -> subscriptionUtils.doOnValidateSubscriptionError(throwable, dataEvent))
                .doOnNext(subscription -> subscriptionRepository.save(subscription).subscribe())
                .doOnError(throwable -> subscriptionUtils.doOnDBSaveError(throwable, dataEvent))
                .doOnNext(subscriptionUtils::sendSubscriptionFeePaymentEvent)
                .thenEmpty(Mono.empty());

    }

    public Mono<Void> validateSubscription(DataEvent<UUID, SubscriptionData> event) {
        return subscriptionRepository.findById(event.getKey())
                .map(subscription -> {
                    subscription.setActive(true);
                    subscription.setPayment(event.getData().getPayment());
                    return subscription;
                })
                .flatMap(subscriptionRepository::save)
                .doOnSuccess(subscriptionUtils::sendAccountCreationEvent)
                .thenEmpty(Mono.empty());
    }
}

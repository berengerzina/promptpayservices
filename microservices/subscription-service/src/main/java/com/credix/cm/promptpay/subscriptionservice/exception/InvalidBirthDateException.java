package com.credix.cm.promptpay.subscriptionservice.exception;

import com.credix.cm.promptpay.commonsutil.exception.GenericException;
import com.credix.cm.promptpay.subscriptionservice.model.Subscription;

/**
 * @author Berenger Zina
 * created on 22/07/2021
 * Project promptpay-services
 **/

public class InvalidBirthDateException extends GenericException {
    public InvalidBirthDateException(Subscription subscription) {
        this.subscription = subscription;
    }

    private Subscription subscription;

    @Override
    public Subscription getEntity() {
        return subscription;
    }
}

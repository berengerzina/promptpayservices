package com.credix.cm.promptpay.subscriptionservice.util;

import com.credix.cm.promptpay.commonsstream.event.DataEvent;
import com.credix.cm.promptpay.commonsstream.event.EventType;
import com.credix.cm.promptpay.commonsstream.event.data.AccountData;
import com.credix.cm.promptpay.commonsstream.event.data.PaymentData;
import com.credix.cm.promptpay.commonsstream.event.data.SubscriptionData;
import com.credix.cm.promptpay.commonsstream.response.ErrorResponse;
import com.credix.cm.promptpay.commonsstream.utils.AbstractStreamBridgeAwareBean;
import com.credix.cm.promptpay.subscriptionservice.model.Subscription;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.UUID;

import static com.credix.cm.promptpay.commonsstream.stream.StreamNamesConstant.ACCOUNT_STREAM_NAME;
import static com.credix.cm.promptpay.commonsstream.stream.StreamNamesConstant.PAYMENT_STREAM_NAME;

/**
 * @author Berenger Zina
 * created on 22/07/2021
 * Project promptpay-services
 **/

@Slf4j
@Component
public class SubscriptionUtils extends AbstractStreamBridgeAwareBean {

    public Mono<Subscription> fromSubscriptionDataToSubscriptionMono(SubscriptionData subscriptionData) {
        var subscription = Subscription.subscriptionBuilder().clientAddress(subscriptionData.getClientAddress())
                .clientBirthdate(subscriptionData.getClientBirthdate())
                .clientCNIExpiration(subscriptionData.getClientCNIExpiration())
                .clientCNINumber(subscriptionData.getClientCNINumber())
                .clientName(subscriptionData.getClientName())
                .phoneNumber(subscriptionData.getPhoneNumber())
                .paymentMethod(subscriptionData.getPaymentMethod())
                .deviseISO(subscriptionData.getDeviseISO())
                .active(false)
                .build();
        return Mono.just(subscription);
    }

    public void validateSubscription(Subscription subscription) {
        if (LocalDate.now().isBefore(subscription.getClientBirthdate())) {
            throw new RuntimeException(); //Todo change with custom exception
        }
        if (Objects.isNull(subscription.getId())) {
            throw new RuntimeException(); //Todo change with custom exception
        }
    }

    public void sendSubscriptionFeePaymentEvent(Subscription subscription) {
        var paymentData = PaymentData.builder()
                .phoneNumber(subscription.getPhoneNumber())
                .transactionAmount(this.retrieveSubscriptionFee())
                .originalEventId(subscription.getId())
                .originalEventType(EventType.SUBSCRIPTION)
                .paymentMethod(subscription.getPaymentMethod())
                .deviseISO(subscription.getDeviseISO())
                .note("PromptPay")
                .message("PromptPay-Subscription")
                .build();
        log.info("payment data for subscription {}", paymentData);
        var dataEvent = new DataEvent<>(UUID.randomUUID(), paymentData, LocalDateTime.now(), EventType.SUBSCRIPTION_FEE_PAYMENT);
        log.info("Sending payment event for subscription {}", dataEvent);
        this.getStreamBridge().send(PAYMENT_STREAM_NAME, dataEvent);
    }

    private BigDecimal retrieveSubscriptionFee() {
        return BigDecimal.valueOf(150L); //ToDo remove this
    }

    public void sendAccountCreationEvent(Subscription subscription) {
        var accountData = AccountData.builder().amount(BigDecimal.ZERO)
                .subscription(subscription)
                .build();
        var dataEvent = new DataEvent<>(UUID.randomUUID(), accountData, LocalDateTime.now(), EventType.ACCOUNT_CREATION);
        log.info("Sending account creation event for subscription {}", subscription.getId());
        this.getStreamBridge().send(ACCOUNT_STREAM_NAME, dataEvent);
    }

    @SneakyThrows
    public void doOnValidateSubscriptionError(Throwable throwable, DataEvent<UUID, SubscriptionData> dataEvent) {
        log.error("An error occurred during subscription details validation", throwable);
        var error = ErrorResponse.builder().code(dataEvent.getEventType().name())
                .message(throwable.getMessage())
                .build();
        this.sendNotificationEvent(dataEvent, new ObjectMapper().writeValueAsString(error));
    }

    @SneakyThrows
    public void doOnDBSaveError(Throwable throwable, DataEvent<UUID, SubscriptionData> dataEvent) {
        log.error("An error occurred during subscription creation", throwable);
        var error = ErrorResponse.builder().code(dataEvent.getEventType().name())
                .message(throwable.getMessage())
                .build();
        this.sendNotificationEvent(dataEvent, new ObjectMapper().writeValueAsString(error));
    }
}

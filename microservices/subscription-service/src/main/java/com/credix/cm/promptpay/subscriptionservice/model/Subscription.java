package com.credix.cm.promptpay.subscriptionservice.model;

import com.credix.cm.promptpay.commonsmodel.models.subscription.SubscriptionLite;
import com.credix.cm.promptpay.commonsmodel.models.transaction.TransactionLite;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.elasticsearch.annotations.DateFormat;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.time.LocalDate;

/**
 * @author Berenger Zina
 * created on 22/07/2021
 * Project promptpay-services
 **/

@Getter @Setter
@EqualsAndHashCode(callSuper = true)
@Document(indexName = "subscription")
public class Subscription extends SubscriptionLite {
    private String clientCNINumber;
    @Field(type = FieldType.Date, format = DateFormat.date)
    private LocalDate clientCNIExpiration;
    private boolean active;

    @Builder(builderMethodName = "subscriptionBuilder")
    public Subscription(String phoneNumber, String clientName, LocalDate clientBirthdate, String clientAddress, TransactionLite payment,
                        String clientCNINumber, LocalDate clientCNIExpiration, boolean active, String deviseISO, String paymentMethod) {
        super(phoneNumber, clientName, clientBirthdate, clientAddress, payment, paymentMethod, deviseISO);
        this.clientCNINumber = clientCNINumber;
        this.clientCNIExpiration = clientCNIExpiration;
        this.active = active;
    }
}

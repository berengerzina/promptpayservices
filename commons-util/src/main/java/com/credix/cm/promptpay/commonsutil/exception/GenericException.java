package com.credix.cm.promptpay.commonsutil.exception;

import com.credix.cm.promptpay.commonsmodel.models.generic.GenericEntity;

/**
 * @author Berenger Zina
 * created on 22/07/2021
 * Project promptpay-services
 **/

public abstract class GenericException extends RuntimeException {
    public GenericException() {
        super();
    }

    public GenericException(String message) {
        super(message);
    }

    public abstract <T extends GenericEntity> T getEntity();
}

package com.credix.cm.promptpay.commonsmodel.models.transaction;

/**
 * @author beren
 * created on 25/07/2021
 * Project promptpay-services
 **/
public enum EPaymentType {
    CASH_IN, CASH_OUT, SUBSCRIPTION_FEES, TRANSFER
}

package com.credix.cm.promptpay.commonsmodel.models.transaction;

/**
 * @author beren
 * created on 27/07/2021
 * Project promptpay-services
 **/
public enum EPaymentSens {
    DEBIT, CREDIT
}

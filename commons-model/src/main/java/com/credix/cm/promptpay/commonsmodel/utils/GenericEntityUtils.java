package com.credix.cm.promptpay.commonsmodel.utils;

import com.credix.cm.promptpay.commonsmodel.models.generic.GenericEntity;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.UUID;

@Component
public class GenericEntityUtils {

    public <T extends GenericEntity> T fillEntityGenericInfos(T entity, UUID id, LocalDateTime createdOn){
        entity.setId(id);
        entity.setCreatedOn(createdOn);
        return entity;
    }
}

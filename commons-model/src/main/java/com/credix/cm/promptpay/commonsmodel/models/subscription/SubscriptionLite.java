package com.credix.cm.promptpay.commonsmodel.models.subscription;

import com.credix.cm.promptpay.commonsmodel.models.generic.GenericEntity;
import com.credix.cm.promptpay.commonsmodel.models.transaction.TransactionLite;
import lombok.*;
import org.springframework.data.elasticsearch.annotations.DateFormat;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.time.LocalDate;

@Builder
@Data
@EqualsAndHashCode(of = {"phoneNumber"}, callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class SubscriptionLite extends GenericEntity {
    private String phoneNumber;
    private String clientName;
    @Field(type = FieldType.Date, format = DateFormat.date)
    private LocalDate clientBirthdate;
    private String clientAddress;
    @Field(type = FieldType.Nested, includeInParent = true)
    private TransactionLite payment;
    private String paymentMethod;
    private String deviseISO;

}

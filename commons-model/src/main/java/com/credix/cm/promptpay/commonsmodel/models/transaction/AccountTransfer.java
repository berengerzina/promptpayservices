package com.credix.cm.promptpay.commonsmodel.models.transaction;

import com.credix.cm.promptpay.commonsmodel.models.generic.GenericEntity;
import lombok.*;
import org.springframework.data.elasticsearch.annotations.DateFormat;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

/**
 * @author beren
 * created on 27/07/2021
 * Project promptpay-services
 **/

@Builder
@Data
@EqualsAndHashCode(of = {"sourceAccountNumber", "destinationAccountNumber", "amount", "paymentDate"}, callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@ToString(of = {"sourceAccountNumber", "destinationAccountNumber", "amount", "paymentDate"})
public class AccountTransfer extends GenericEntity implements Cloneable{
    protected BigDecimal amount;
    protected UUID destinationAccountId;
    protected UUID sourceAccountId;
    protected String destinationAccountNumber;
    protected String sourceAccountNumber;
    @Field(type = FieldType.Date, format = DateFormat.date_hour_minute_second)
    protected LocalDateTime paymentDate;
    private EPaymentSens paymentSens;

    @SneakyThrows
    public AccountTransfer cloneAccountTransfer(){
        return (AccountTransfer) this.clone();
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}

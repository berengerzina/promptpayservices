package com.credix.cm.promptpay.commonsmodel.models.transaction;

import lombok.*;
import org.springframework.data.elasticsearch.annotations.DateFormat;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class TransactionLite extends AccountTransfer {
    private EPaymentState paymentState;
    private EPaymentType paymentType;

    @Builder(builderMethodName = "transactionLiteBuilder")
    public TransactionLite(AccountTransfer accountTransfer, EPaymentState paymentState, EPaymentType paymentType) {
        super(accountTransfer.getAmount(), accountTransfer.destinationAccountId, accountTransfer.sourceAccountId,
                accountTransfer.destinationAccountNumber, accountTransfer.sourceAccountNumber, accountTransfer.paymentDate, null);
        this.paymentState = paymentState;
        this.paymentType = paymentType;
    }

    public TransactionLite(TransactionLite transactionLite) {
        super(transactionLite.getAmount(), transactionLite.destinationAccountId, transactionLite.sourceAccountId,
                transactionLite.destinationAccountNumber, transactionLite.sourceAccountNumber, transactionLite.paymentDate, null);
        this.paymentState = transactionLite.getPaymentState();
        this.paymentType = transactionLite.getPaymentType();
    }
}

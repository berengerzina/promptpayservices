package com.credix.cm.promptpay.commonsmodel.models.generic;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.DateFormat;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

@Data	
@EqualsAndHashCode(of = "id")
public abstract class GenericEntity implements Serializable {

    @Id
    protected UUID id;
    @Field(type = FieldType.Date, format = DateFormat.date_hour_minute_second)
    protected LocalDateTime createdOn;
    protected Boolean deleted=false;

}

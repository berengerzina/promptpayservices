package com.credix.cm.promptpay.commonsmodel.models.transaction;

public enum EPaymentState {
    CREATED, INITIATED, VALIDATED, FAILED;
}

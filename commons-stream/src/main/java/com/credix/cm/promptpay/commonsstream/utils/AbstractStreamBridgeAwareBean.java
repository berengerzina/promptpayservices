package com.credix.cm.promptpay.commonsstream.utils;

import com.credix.cm.promptpay.commonsstream.event.DataEvent;
import com.credix.cm.promptpay.commonsstream.event.data.AbstractData;
import com.credix.cm.promptpay.commonsstream.event.data.NotificationData;
import com.credix.cm.promptpay.commonsstream.stream.StreamNamesConstant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.function.StreamBridge;

import java.util.UUID;

/**
 * @author Berenger Zina
 * created on 22/07/2021
 * Project promptpay-services
 **/

@Slf4j
public abstract class AbstractStreamBridgeAwareBean {
    private StreamBridge streamBridge;

    @Autowired
    public void setStreamBridge(StreamBridge streamBridge) {
        this.streamBridge = streamBridge;
    }

    public StreamBridge getStreamBridge() {
        return streamBridge;
    }

    protected void sendNotificationEvent(DataEvent<UUID, ? extends AbstractData> event, String responseBody) {
        log.info("Sending notification for event type {} with event id {}", event.getEventType(), event.getKey());
        var notification = NotificationData.builder().notificationType(event.getEventType())
                .phoneNumber(event.getData().getPhoneNumber())
                .responseBody(responseBody)
                .build();
        this.getStreamBridge().send(StreamNamesConstant.NOTIFICATION_STREAM_NAME, notification);
    }
}

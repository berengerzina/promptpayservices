package com.credix.cm.promptpay.commonsstream.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * @author Berenger Zina
 * created on 22/07/2021
 * Project promptpay-services
 **/

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class IncomingTransferResponse {
    private String accountNumber;
    private BigDecimal balance;
    private BigDecimal amount;
    private String transferOriginHolder;
}

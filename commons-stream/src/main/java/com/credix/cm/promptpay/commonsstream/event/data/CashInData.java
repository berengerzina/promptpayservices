package com.credix.cm.promptpay.commonsstream.event.data;

import com.credix.cm.promptpay.commonsmodel.models.transaction.TransactionLite;
import lombok.*;

import java.math.BigDecimal;

/**
 * @author beren
 * created on 23/07/2021
 * Project promptpay-services
 **/

@Getter
@Setter
@ToString(of = {"cashInAmount"}, callSuper = true)
@EqualsAndHashCode(callSuper = true, of = {"accountNumber", "cashInAmount"})
public class CashInData extends AbstractData{
    private String accountNumber;
    private BigDecimal cashInAmount;
    private TransactionLite payment;

    @Builder
    public CashInData(String phoneNumber, BigDecimal cashInAmount, TransactionLite payment, String accountNumber) {
        super(0L, phoneNumber);
        this.cashInAmount = cashInAmount;
        this.payment = payment;
        this.accountNumber = accountNumber;
    }
}

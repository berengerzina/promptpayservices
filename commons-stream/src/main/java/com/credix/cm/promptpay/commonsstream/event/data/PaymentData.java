package com.credix.cm.promptpay.commonsstream.event.data;

import com.credix.cm.promptpay.commonsmodel.models.transaction.EPaymentType;
import com.credix.cm.promptpay.commonsstream.event.EventType;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import java.math.BigDecimal;
import java.util.UUID;

/**
 * @author Berenger Zina
 * created on 22/07/2021
 * Project promptpay-services
 **/

@Getter
@Setter
@ToString(of = {"transactionAmount", "paymentMethod", "paymentType", "sourceAccountId", "destinationAccountId"}, callSuper = true)
@JsonIgnoreProperties(ignoreUnknown = true)
@EqualsAndHashCode(callSuper = true, of = "originalEventId")
public class PaymentData extends AbstractData {
    private BigDecimal transactionAmount;
    private String paymentMethod;
    private UUID originalEventId;
    private EventType originalEventType;
    private String note;
    private String message;
    private String deviseISO;
    private UUID destinationAccountId;
    private UUID sourceAccountId;
    private String destinationAccountNumber;
    private String sourceAccountNumber;
    private EPaymentType paymentType;

    @Builder
    public PaymentData(Long numberOfRetry, String phoneNumber, BigDecimal transactionAmount,
                       String paymentMethod, UUID originalEventId, EventType originalEventType,
                       String note, String message, String deviseISO, UUID destinationAccountId,
                       UUID sourceAccountId, String destinationAccountNumber, String sourceAccountNumber, EPaymentType paymentType) {
        super(numberOfRetry, phoneNumber);
        this.transactionAmount = transactionAmount;
        this.paymentMethod = paymentMethod;
        this.originalEventId = originalEventId;
        this.originalEventType = originalEventType;
        this.note = note;
        this.message = message;
        this.deviseISO = deviseISO;
        this.destinationAccountId = destinationAccountId;
        this.sourceAccountId = sourceAccountId;
        this.paymentType = paymentType;
        this.destinationAccountNumber = destinationAccountNumber;
        this.sourceAccountNumber = sourceAccountNumber;
    }
}

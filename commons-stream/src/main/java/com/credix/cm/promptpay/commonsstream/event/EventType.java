package com.credix.cm.promptpay.commonsstream.event;

/**
 * @author Berenger Zina
 * created on 22/07/2021
 * Project promptpay-services
 **/

public enum EventType {
    SUBSCRIPTION, SUBSCRIPTION_VALIDATION, SUBSCRIPTION_FEE_PAYMENT, ACCOUNT_CREATION, PAYMENT_VALIDATION,
    CASH_IN, CASH_IN_PAYMENT, CASH_IN_VALIDATION, TRANSFER, TRANSFER_VALIDATION
}

package com.credix.cm.promptpay.commonsstream.stream;

/**
 * @author Berenger Zina
 * created on 22/07/2021
 * Project promptpay-services
 **/

public class StreamNamesConstant {
    public static final String PAYMENT_STREAM_NAME = "payment_stream";
    public static final String SUBSCRIPTION_STREAM_NAME = "subscription_stream";
    public static final String ACCOUNT_STREAM_NAME = "account_stream";
    public static final String PAYMENT_VALIDATION_STREAM_NAME = "payment_validation_stream";
    public static final String PAYMENT_VALIDATION_DLQ_STREAM_NAME = "payment_validation_stream.dlq";
    public static final String NOTIFICATION_STREAM_NAME = "notification_stream";
    public static final String PAYMENT_VALIDATION_ERROR_STREAM_NAME = "payment_validation_error_stream";
    public static final String CASH_IN_STREAM_NAME = "cash_in_stream";
    public static final String CASH_IN_ERROR_STREAM_NAME = "cash_in_error_stream";
    public static final String TRANSFER_STREAM_NAME = "transfer_stream";

    private StreamNamesConstant() {
    }
}

package com.credix.cm.promptpay.commonsstream.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

/**
 * @author Berenger Zina
 * created on 22/07/2021
 * Project promptpay-services
 **/

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ErrorResponse  extends AbstractResponse {
    private String code;
    private String message;

    @Builder
    public ErrorResponse(String accountNumber, String code, String message) {
        super(accountNumber);
        this.code = code;
        this.message = message;
    }
}

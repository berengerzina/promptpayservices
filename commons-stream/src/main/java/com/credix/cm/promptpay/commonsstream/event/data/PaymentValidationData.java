package com.credix.cm.promptpay.commonsstream.event.data;

import com.credix.cm.promptpay.commonsstream.event.EventType;
import lombok.*;

import java.util.UUID;

/**
 * @author Berenger Zina
 * created on 22/07/2021
 * Project promptpay-services
 **/

@Getter
@Setter
@NoArgsConstructor
@ToString(of = {"transactionId", "originalEventId", "originalEventType"}, callSuper = true)
public class PaymentValidationData extends AbstractData{
    private UUID transactionId;
    private UUID originalEventId;
    private EventType originalEventType;


    @Builder
    public PaymentValidationData(String phoneNumber, UUID transactionId, UUID originalEventId, EventType originalEventType) {
        super(0L, phoneNumber);
        this.transactionId = transactionId;
        this.originalEventId = originalEventId;
        this.originalEventType = originalEventType;
    }
}

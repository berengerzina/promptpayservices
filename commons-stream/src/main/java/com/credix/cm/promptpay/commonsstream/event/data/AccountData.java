package com.credix.cm.promptpay.commonsstream.event.data;

import com.credix.cm.promptpay.commonsmodel.models.subscription.SubscriptionLite;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import java.math.BigDecimal;

/**
 * @author Berenger Zina
 * created on 22/07/2021
 * Project promptpay-services
 **/

@Getter
@Setter
@ToString(of = {"phoneNumber", "amount", "subscription"})
@EqualsAndHashCode(of = "subscriptionId")
@JsonIgnoreProperties(ignoreUnknown = true)
public class AccountData extends AbstractData {
    private BigDecimal amount;
    private SubscriptionLite subscription;

    @Builder
    public AccountData(String phoneNumber, BigDecimal amount, SubscriptionLite subscription) {
        super(0L, phoneNumber);
        this.amount = amount;
        this.subscription = subscription;
    }
}

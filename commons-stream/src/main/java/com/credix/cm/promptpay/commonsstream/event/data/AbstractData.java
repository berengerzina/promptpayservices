package com.credix.cm.promptpay.commonsstream.event.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author Berenger Zina
 * created on 22/07/2021
 * Project promptpay-services
 **/

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString(of = {"phoneNumber", "numberOfRetry"})
public abstract class AbstractData {
    private Long numberOfRetry;
    private String phoneNumber;
}

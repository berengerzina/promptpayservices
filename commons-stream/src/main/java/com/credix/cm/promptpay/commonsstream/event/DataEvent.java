package com.credix.cm.promptpay.commonsstream.event;

import com.credix.cm.promptpay.commonsstream.event.data.AbstractData;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDateTime;

/**
 * @author Berenger Zina
 * created on 22/07/2021
 * Project promptpay-services
 **/

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(of = {"key", "data", "eventDate", "eventType"})
public class DataEvent<K, T extends AbstractData> {
    private K key;
    private T data;
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime eventDate;
    private EventType eventType;
}

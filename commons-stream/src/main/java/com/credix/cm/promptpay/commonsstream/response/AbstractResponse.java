package com.credix.cm.promptpay.commonsstream.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

/**
 * @author Berenger Zina
 * created on 22/07/2021
 * Project promptpay-services
 **/

@Data
@AllArgsConstructor
public abstract class AbstractResponse {
    private String accountNumber;
}

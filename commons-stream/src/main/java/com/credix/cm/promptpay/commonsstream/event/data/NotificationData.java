package com.credix.cm.promptpay.commonsstream.event.data;

import com.credix.cm.promptpay.commonsstream.event.EventType;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

/**
 * @author Berenger Zina
 * created on 22/07/2021
 * Project promptpay-services
 **/

@Getter
@Setter
@ToString(of = {"notificationType", "responseBody"}, callSuper = true)
@EqualsAndHashCode(callSuper = true)
@JsonIgnoreProperties(ignoreUnknown = true)
public class NotificationData extends AbstractData {
    private EventType notificationType;
    private String responseBody;

    @Builder
    public NotificationData(String phoneNumber, EventType notificationType, String responseBody) {
        super(0L, phoneNumber);
        this.notificationType = notificationType;
        this.responseBody = responseBody;
    }
}

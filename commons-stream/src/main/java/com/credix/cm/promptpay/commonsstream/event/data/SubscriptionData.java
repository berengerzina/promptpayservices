package com.credix.cm.promptpay.commonsstream.event.data;

import com.credix.cm.promptpay.commonsmodel.models.transaction.TransactionLite;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDate;

/**
 * @author Berenger Zina
 * created on 22/07/2021
 * Project promptpay-services
 **/

@Getter
@Setter
@ToString(of = {"clientName", "clientBirthdate", "clientCNINumber", "clientCNIExpiration"})
public class SubscriptionData extends AbstractData {
    private String clientName;
    private LocalDate clientBirthdate;
    private String clientAddress;
    private String clientCNINumber;
    private LocalDate clientCNIExpiration;
    private TransactionLite payment;
    private String deviseISO;
    private String paymentMethod;

    @Builder
    public SubscriptionData(String phoneNumber, String clientName, LocalDate clientBirthdate, String clientAddress,
                            String clientCNINumber, LocalDate clientCNIExpiration, TransactionLite payment, String deviseISO, String paymentMethod) {
        super(0L, phoneNumber);
        this.clientName = clientName;
        this.clientBirthdate = clientBirthdate;
        this.clientAddress = clientAddress;
        this.clientCNINumber = clientCNINumber;
        this.clientCNIExpiration = clientCNIExpiration;
        this.payment = payment;
        this.deviseISO = deviseISO;
        this.paymentMethod = paymentMethod;
    }
}

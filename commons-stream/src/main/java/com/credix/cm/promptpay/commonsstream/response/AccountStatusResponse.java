package com.credix.cm.promptpay.commonsstream.response;

import lombok.*;

import java.math.BigDecimal;

/**
 * @author Berenger Zina
 * created on 22/07/2021
 * Project promptpay-services
 **/

@Getter
@Setter
public class AccountStatusResponse extends AbstractResponse {
    private String accountHolderName;
    private BigDecimal balance;

    @Builder
    public AccountStatusResponse(String accountNumber, String accountHolderName, BigDecimal balance) {
        super(accountNumber);
        this.accountHolderName = accountHolderName;
        this.balance = balance;
    }
}

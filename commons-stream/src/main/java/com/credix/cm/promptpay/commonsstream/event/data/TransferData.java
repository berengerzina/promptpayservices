package com.credix.cm.promptpay.commonsstream.event.data;

import com.credix.cm.promptpay.commonsmodel.models.transaction.AccountTransfer;
import com.credix.cm.promptpay.commonsmodel.models.transaction.TransactionLite;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

/**
 * @author beren
 * created on 23/07/2021
 * Project promptpay-services
 **/
@Getter
@Setter
@EqualsAndHashCode(of = {"sourceAccount", "destinationAccount", "transferAmount"}, callSuper = true)
public class TransferData extends AbstractData {
    private String sourceAccount;
    private String destinationAccount;
    private BigDecimal transferAmount;
    private AccountTransfer accountTransfer;

    @Builder
    public TransferData(Long numberOfRetry, String phoneNumber, String sourceAccount, String destinationAccount,
                        BigDecimal transferAmount, AccountTransfer accountTransfer) {
        super(numberOfRetry, phoneNumber);
        this.sourceAccount = sourceAccount;
        this.destinationAccount = destinationAccount;
        this.transferAmount = transferAmount;
        this.accountTransfer = accountTransfer;
    }
}
